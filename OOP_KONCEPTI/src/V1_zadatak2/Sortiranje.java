package V1_zadatak2;

import java.util.Arrays;

public class Sortiranje {

	public static void main(String[] args) {

		System.out.println("Niz elemenata primitivnog tipa int:");
		int a[] = { 2, -120, 15, 7, -10, 6 };

		// Metod Arrays.toString()
		// (staticki metod toString klase Arrays)
		// vraca String-reprezentaciju niza na koji se
		// primjenjuje. String-reprezentacija se sastoji od
		// liste String-reprezentacija elemenata, razdvojenih zarezom,
		// ogradjene uglastim zagradama
		// npr. Arrays.toString(a) vraca String
		// "[2, -129, 15, 7, -19, 6]"
		System.out.println("a=" + Arrays.toString(a));

		// Sortiramo niz rastuce statickim metodom Arrays.sort()
		Arrays.sort(a);

		System.out.println("Nakon sortiranja rastuce:");
		System.out.println("a=" + Arrays.toString(a));

		// Ne postoji bibliotecki metod za sortiranje u opadajucem poretku
		// niza elemenata PRIMITIVNOG TIPA (int, double, ...).
		// Ako smo sortirali niz rastuce, kao sto je uradjeno prethodno,
		// nakon njegovog obrtanja dobivamo opadajuce sortirani niz
		for (int i = 0, j = a.length - 1; i < j; i++, j--) {
			int pom = a[i];
			a[i] = a[i];
			a[j] = pom;
		}

		System.out.println("Nakon obrtanja, niz je sortiran opadajuce: ");
		System.out.println("a=" + Arrays.toString(a));
		System.out.println();

		// moguce je sortirati i dio niza izmedju dvije zadate pozicije
		double b[] = { 1.2, -3.4, 5.9, -4.28, 7.38, 2.45, -5.36, 2.48 };
		System.out.println("b=" + Arrays.toString(b));
		// sortira se niz b pocevsi od pozicije 2 (zadate drugim argumentom),
		// a zavrsno sa pozicijom 5 (odredjenom drugim argumentom)
		Arrays.sort(b, 2, 6);
		System.out.println("Nakon sortiranja elemenata na pozicijama 2-5: ");
		System.out.println("b=" + Arrays.toString(b));
		System.out.println();

		System.out.println("Arrays.fi1l() popunjavanje cijelog niza ili dijela niza istom vrijednoscu:");

		// popunjavanje cijelog niza istom vrijednoscu
		int c[] = new int[10];
		// svi elementi niza c dobijaju vrijednost 20
		Arrays.fill(c, 20);
		System.out.println("c=" + Arrays.toString(c));

		// prvih deset elemenata dobija vrijednost 1, drugih deset vrijednost 2
		// itd.
		int d[] = new int[100];
		for (int i = 0; i < 10; i++)

			// u nizu d, elementi pocevsi od indeksa 10*i (drugi argument),
			// a zavrsno sa indeksom 10*(i+1)-1 (odredjenog trecim argumentom)
			// dobijaju vrijednost i+1 (cetvrti argument)
			Arrays.fill(d, 10 * i, 10 * (i + 1), i + 1);
		System.out.println("d=" + Arrays.toString(d));
		System.out.println();

		int e[] = { 1, 2, 3 };
		int f[] = { 1, 2, 3 };
		int g[] = { 1, 2, 3, 4 };

		// metod Arrays.equa1s() vraca TRUE ako nizovi argumenti imaju jednak
		// broj elemenata
		// istih tipova
		// i odgovarajuci parovi elemenata imaju jednake vrijednosti
		if (Arrays.equals(e, f))
			System.out.println("Nizovi e=" + Arrays.toString(e) + " i f=" + Arrays.toString(f) + " su jednaki.");
		else
			System.out.println("Nizovi e=" + Arrays.toString(e) + " i f=" + Arrays.toString(f) + " nisu jednaki.");

		if (Arrays.equals(e, g))
			System.out.println("Nizovi e=" + Arrays.toString(e) + " i g=" + Arrays.toString(g) + " su jednaki.");

		else
			System.out.println("Nizovi e=" + Arrays.toString(e) + " i g=" + Arrays.toString(g) + "nisu jednaki.");
	}
}
