package V4_zadatak4;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class Prozor2 {

	public static void main(String[] args) {
		// prozor objekat
		JFrame prozor = new JFrame("Centrirani prozor");

		// toolkit sadrzi informacije o okruzenju u kome
		// se program izvrsava, ukljucujuci velicinu ekrana u pikselima
		Toolkit theKit = prozor.getToolkit();
		Dimension vEk = theKit.getScreenSize();

		prozor.setBounds(vEk.width / 4, vEk.height / 4, // pozicija
						vEk.width / 2, vEk.height / 2); // velicina
		prozor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		prozor.setVisible(true);
	}

}
