package V4_zadatak4;

import java.awt.GraphicsEnvironment;
import java.awt.Point;

import javax.swing.JFrame;

public class Prozor3 {

	// prozor objekat
	public static JFrame prozor = new JFrame("Centrirani prozor");

	public static void main(String[] args) {

		// GraphicEnviroment objekat sadrzi informacije o
		// grafickim uredjajima u sistemu
		// (izmedju ostalog i o centralnoj tacki
		Point centar = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();

		int sirinaProzora = 400;
		int visinaProzora = 150;
		prozor.setBounds(centar.x - sirinaProzora / 2, centar.y - visinaProzora / 2, sirinaProzora, visinaProzora);
		prozor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		prozor.setVisible(true);
	}
}
