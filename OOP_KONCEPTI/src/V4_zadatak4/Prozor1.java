package V4_zadatak4;

import javax.swing.JFrame;

public class Prozor1 {
	// objekat prozor
	private static JFrame prozor = new JFrame("Naslov prozora");

	public static void main(String[] args) {
		int sirProz = 400; // sirina prozora u pikselima
		int visProz = 150; // visina prozora u pikselima

		prozor.setBounds(50, 100, sirProz, visProz);
		// postavljanje pozicije i velicine
		prozor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		prozor.setVisible(true);
	}
}
