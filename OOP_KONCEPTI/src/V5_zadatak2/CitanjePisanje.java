package V5_zadatak2;

// za pisanje u fajl
//import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

// za citanje iz fajla
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CitanjePisanje {

	public static void main(String[] args) {
		/*
		 * koristimo try-with-resources i deklarisemo oba resursa, Scanner i
		 * PrintWriter objekat
		 */

		try (Scanner sc = new Scanner(new File("brojevi.txt"));
				PrintWriter izl = new PrintWriter(new File("sredina.txt"))) {
			/* resursi ce biti automatski zatvoreni po izlasku iz try
			 * (bilo normalno, bilo zbog izbacivanja izuzetka)
			 * obrnutim redom od onog kojim su deklarisani
			 */
			
			// citamo vrijednosti, dodajemo ih sumi i pamtimo njihov ukupan broj
			double suma = 0;
			int koliko = 0;
			
			while (sc.hasNextDouble()) {
				suma += sc.nextDouble();
				koliko++;
			}
			
			/* posanje u fajl */
			
			// ispisujemo prosjek
			if (koliko > 0) {
				izl.println("Srednja vrijednost = " + suma / koliko);
			} else {
				izl.println("Nema unesenih vrijednosti.");
			}

		} catch (FileNotFoundException e) {
			System.out.println("Greska pri otvaranju fajla \"brojevi.txt\" za citanje");
			System.exit(1);
		} catch (IOException e) {
			System.out.println("Neuspjela operacija pripreme za upis u fajl \"sredina.txt\"");
			System.exit(1);
		}

	}
}
