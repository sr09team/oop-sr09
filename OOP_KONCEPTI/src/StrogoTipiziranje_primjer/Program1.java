package StrogoTipiziranje_primjer;

public class Program1 {

	public static void main(String[] args) {
		
		Window myWindow1=new Window();
		Window myWindow2=new Window();
		
		myWindow1=new RectangularWindow(120,100);
		myWindow1.setMaterial(myWindow1.materialList[0]);
		myWindow1.calculateArea();
		
		myWindow2=new RoundWindow(100);
		myWindow2.setMaterial(myWindow2.materialList[3]);
		myWindow2.calculateArea();
				
		//prikazi  prozore
		System.out.println("myWindow1 je od " + myWindow1.getMaterial());
		System.out.println("myWindow2 je od " + myWindow2.getMaterial());
	}

}
