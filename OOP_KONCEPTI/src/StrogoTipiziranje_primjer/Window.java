package StrogoTipiziranje_primjer;

public class Window {
	//varijable
	public String id;
	public double width=0;
	public double height=0;
	public double area=0;
	public String material;
	public String materialList[] = {"Wood","Aluminium","Wood-Aluminium","Plastic"};
	
	//defaultni konstruktor
	public Window() {
	}
	
	//metoda izracunavanja povrsine
	public void calculateArea(){
	}
	
	//metoda koja vraca povrsinu
	public double getArea() {
		return area;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getMaterial() {
		return material;
	}
	
	
	
	
	
}
