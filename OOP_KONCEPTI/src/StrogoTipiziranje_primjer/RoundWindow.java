package StrogoTipiziranje_primjer;

public class RoundWindow extends Window {
	public String id;
	public double radius=0.0;
	public String material="";//timber
	
	//defaultni konstruktor
	public RoundWindow() {
	}

	// specijalni konstruktor sa vrijednostima
	public RoundWindow(double iRadius) {
		radius = iRadius;
	}

	public RoundWindow(String iMaterial, double iRadius) {
		radius = iRadius;
		material = iMaterial;
	}
	
	public void calculateArea(){
		area=Math.PI*radius*radius;
	}
}
