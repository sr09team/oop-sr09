package V3_zadatak1;

public class GenerickiMetod {
	//genericki metod za ispis razlicitih nizova

	public static <E> void ispisNiza (E [] ulazniNiz){
		for (E element:ulazniNiz){
			System.out.printf("%s", element);
			System.out.println();
		}
	}

	/*glavni program - incijalizacija razlicitih nizova
	* i poziv istog mtoda (generickog) za ispis niza */
	public static void main(String args[]){
		Integer [] ulazniNiz1= {1, 2, 3, 4, 5};
		Double [] ulazniNiz2= { 1.1, 1.2, 3.1, 4.4};
		Character [] ulazniNiz3 = {'a', 'B', 'c'};
		
		System.out.println("Ulazni niz1:");
		ispisNiza(ulazniNiz1);

		System.out.println("U1azni niz2:");
		ispisNiza(ulazniNiz2);

		System.out.println ("Ulazni niz3:");
		ispisNiza(ulazniNiz3);
	}
}
