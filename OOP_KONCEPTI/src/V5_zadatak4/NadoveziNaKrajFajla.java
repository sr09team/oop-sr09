package V5_zadatak4;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class NadoveziNaKrajFajla {

	public static void main(String[] args) {

		/* ukoliko fajl datog imena postoji, ono sto pisemo nadovezuje se na
		 * kraj njegovog sadrzaja (true kao drugi argument konstruktora FileWriter)
		 */
		try(PrintWriter izl = new PrintWriter(new FileWriter ("nadovezivanje.txt",true))){
			for (int i=0;i<10;i++) izl.println(i+1+". red");
			izl.println();
		}catch (IOException e){
			System.out.println("Greska pri otvaranju fajla \"nadovezivanje.txt\" za pisanje");
		}
		
		// RELATIVNE PUTANJE
		/* osim samog imena fajla, kada se podrazumjeva da je njegova lokacija folder projekta
		 * i apsolutne putanje (oblika C:/dir_1/dir_2/../dir_n/ime_fajla ili  C://dir_1//dir_2//..//dir_n//ime_fajla
		 * moguce je koristiti i relative putanje:
		 * npr poddir_1/poddir_2/.../poddir_n/ime_fajla
		 * ili poddir_1\\poddir_2\\...\\poddir_n\\ime_fajla
		 * gdje je poddir_1 poddirektoriji (podfolder) tekuceg direktorija (folder projekta),
		 * poddir_2 njegov poddirektorij itd
		 * 
		 * moguce je, koristeci relativne pitanje, kretati se i navise na isti nacin kao u linux-u
		 * .. oznacava (direktni) naddirektorij
		 * .. notacija se moze koristiti potreban broj puta kako bi se doslo do zeljenog nedirektnog naddirektorija
		 */
		/* nadovezivanje1.txt je u naddirektoriju projekta ( direktoriju koji je izabran za workspace) */
		try(PrintWriter izl = new PrintWriter(new FileWriter ("../nadovezivanje.txt",true))){
			for (int i=0;i<10;i++) izl.println(i+1+". red");
			izl.println();
		}catch (IOException e){
			System.out.println("Greska pri otvaranju fajla \"nadovezivanje.txt\" za pisanje");
		}
		
		/* nadovezivanje2.txt je u naddirektoriju naddirektorija ( direktoriju kje je naddirektorij za workspace,
		 * ako je workspace na desktopu, tamo ce biti i nadovezivanje2.txt
		 */
		try(PrintWriter izl = new PrintWriter(new FileWriter ("../../nadovezivanje.txt",true))){
			for (int i=0;i<10;i++) izl.println(i+1+". red");
			izl.println();
		}catch (IOException e){
			System.out.println("Greska pri otvaranju fajla \"nadovezivanje.txt\" za pisanje");
		}
		/* nadovezivanje3.txt je u poddirektoriju poddir
		 * direktorija projekta. Ovaj poddirektorij mora
		 * da postoji, inace dolazi do izbacivanja izuzetka
		 */
		try(PrintWriter izl = new PrintWriter(new FileWriter ("poddir/nadovezivanje.txt",true))){
			for (int i=0;i<10;i++) izl.println(i+1+". red");
			izl.println();
		}catch (IOException e){
			System.out.println("Greska pri otvaranju fajla \"nadovezivanje.txt\" za pisanje");
		}
	}
	

}
