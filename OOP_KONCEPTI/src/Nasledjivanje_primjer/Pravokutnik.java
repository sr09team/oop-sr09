package Nasledjivanje_primjer;

public class Pravokutnik extends Lik {

	double a,b;
	
	@Override
	double izracunajPovrsinu() {
		return a*b;
	}

	@Override
	public String toString() {
		return "Pravokutnik, a=" + a + ", b=" + b + "\t";
	}
	

}
