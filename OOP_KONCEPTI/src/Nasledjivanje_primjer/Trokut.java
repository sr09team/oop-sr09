package Nasledjivanje_primjer;

public class Trokut extends Lik {
	double a,b,c;
	
	@Override
	double izracunajPovrsinu() {
		double s=(a+b+c)/2;;
		return Math.sqrt(s*(s-a)*(s-b)*(s-c)); 
	}

	@Override
	public String toString() {
		return "Trokut, a=" + a + ", b=" + b + ", c=" + c + "\t";
	}
	

}
