package Nasledjivanje_primjer;

public class Elipsa extends Lik {
	double r1,r2;
	
	@Override
	double izracunajPovrsinu() {
		return r1*r2*Math.PI;
	}

	@Override
	public String toString() {
		return "Elipsa, r1=" + r1 + ", r2=" + r2 + "\t";
	}
	

}
