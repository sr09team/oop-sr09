package V4_zadatak1;

public class RimskiBroj {
private int n; // cjelobrojna decimalna reprezentacija

//konstruktori
//1. konstruktor ima parametar tipa int - arapski broj
public RimskiBroj(int n) {
	this.n = n;
}

// 2. konstruktor koji ima parametar tipa string (broj u rimskom zapisu
//u tijelu konstruktora se odmah vrsi pretvorba u arapski broj
public RimskiBroj(String r) {
	for (int i = 0; i < r.length(); i++)
	switch(r.charAt(i)) {
		case 'm': 
		case 'M': 
			n = n + 1000; 
			break;
		case 'd': 
		case 'D': 
			n = n + 500;
			break;
		case 'c': 
		case 'C': 
			n = n + 100; 
			break;
		case 'l': 
		case 'L':
			n = n + 50;
			break;
		case 'x': 
		case 'X':
			n = n + 10; 
			break;
		case 'v': 
		case 'V':
			n = n + 5;
			break;
		case 'i': 
		case 'I':
			n = n + 1;
			break;
	}
}

// pretvaranje rimskog u decimalni (arapski) broj
public int toInt() {
	return n;
}

// pretvaranje decimalnog u rimski broj
public String toString() {
	return d2r(n);
}
//pomocni rekurzivni metod za pretvaranje
//decimalnog u rimski
private String d2r(int n) {
	if (n >= 1000)
		return "M" + d2r(n - 1000);
	else if (n >= 500)
		return "D" + d2r(n - 500);
	else if (n >= 100)
		return "C" + d2r(n - 100);
	else if (n >= 50)
		return "L" + d2r(n - 50);
	else if (n >= 10)
		return "X" + d2r(n - 10);
	else if (n >= 5)
		return "V" + d2r(n - 5);
	else if (n >= 1)
		return "I" + d2r(n - 1);
	else
		return "";
}

//staticki metod za sabiranje dva rimska broja
// RimskiBroj z = RimskiBroj.zbir(x, y)
public static RimskiBroj zbir(RimskiBroj a, RimskiBroj b) {
	return new RimskiBroj(a.n + b.n);
}

//objektni metod za sabiranje dva rimska broja
// RimskiBroj z = x.dodaj(y);
public RimskiBroj dodaj(RimskiBroj a) {
	return new RimskiBroj(this.n + a.n);
}

//staticki metod za proizvod dva rimska broja
// RimskiBroj 2 = RimskiBroj.proizvod(x, y)

public static RimskiBroj proizvod(RimskiBroj a, RimskiBroj b) {
	return new RimskiBroj(a.n * b.n);
}

//objektni metod za proizvod dva rimska broja
// RimskiBroj 2 = x.pomnozi(y);

public RimskiBroj pomnozi(RimskiBroj a) {
	return new RimskiBroj(this.n * a.n);
}

public static void main(String[] args) {
	RimskiBroj x = new RimskiBroj("xxxiiii"); // 34
	System.out.println("x = " + x.toInt());
	System.out.println("x = " + x); // x.toString()
	RimskiBroj y = new RimskiBroj("mdclxvi"); //1666
	System.out.println("y = " + y.toInt());
	System.out.println("y = " + y); // y.toString()
	System.out.println();
	System.out.println( // RimskiBroj.zbir(x, y).toString() 
			"x+y = " + RimskiBroj.zbir(x, y));
	System.out.println("x+y = " + RimskiBroj.zbir(x, y).toInt());
	System.out.println( // x.dodaj(y).toString()
			"x+y = " + x.dodaj(y));
	System.out.println("x+y = " + x.dodaj(y).toInt());
	System.out.println();
	System.out.println( // RimskiBroj.proizvod(x, y).toString()
			"x*y = " + RimskiBroj.proizvod(x, y));
	System.out.println("x*y = " + RimskiBroj.proizvod(x, y).toInt());
	System.out.println( // x.pomnoii(y).toString()
			"x*y = " + x.pomnozi(y));
	System.out.println("x*y = " + x.pomnozi(y).toInt());
}
}


