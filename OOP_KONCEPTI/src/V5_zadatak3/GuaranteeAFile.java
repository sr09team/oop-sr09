package V5_zadatak3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class GuaranteeAFile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String filename = "C:/Beg Java Stuff/Bonzo/Beanbag/myfile.txt";
		File aFile = new File(filename);	// krairanje File object-a
		// provjera da li je path (putanja) fajl
		if (aFile.isDirectory()){
			// ukoliko je u pitanju putanja do foldera izvrsiti prekid
			System.out.println("The path "+ aFile.getPath()+" doesnt specify the file. Program aborted.");
			System.exit(1);
		}
		// ukoliko je u pitanju fajl, ali isti ne postoji
		if(!aFile.isFile()){
			// provjera roditeljskog foldera
			aFile=aFile.getAbsoluteFile();
			File parentDir = new File(aFile.getParent());
			if(!parentDir.exists()){ // krairanje roditeljskog foldera ukoliko je potrebno
				parentDir.mkdirs();
			}
		}
		FileOutputStream outputFile = null; // postavljanje stream reference
		try{
			// stvaranje stream-a za dodavanje podataka
			outputFile= new FileOutputStream(aFile,true);
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.exit(1);
	}

}
