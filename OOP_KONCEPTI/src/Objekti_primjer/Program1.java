package Objekti_primjer;

public class Program1 {

	public static void main(String[] args) {
		Window myWindow1=new Window();
		myWindow1.width=120;
		myWindow1.height=150;
		myWindow1.calculateArea();
		
		//nova instanca klase Window
		Window myWindow2=null;
		try{
			myWindow2=(Window)(Class.forName("Window").newInstance());
		}
		catch (Exception e){
			System.out.println("Error: " + e);
		}
		
		myWindow2.width=100;
		myWindow2.height=150;
		
		//prikazi velicinu prozora
		System.out.println("myWindow1 ima sirinu " + myWindow1.width);
		System.out.println("myWindow2 ima sirinu " + myWindow2.width);
	}

}
