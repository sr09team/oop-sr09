package Persistencija_primjer;

import java.io.Serializable;

public class RoundWindow extends Window implements Serializable{
	/**
	 * ID za serilized objekat
	 */
	private static final long serialVersionUID = 520643728065029485L;
	//varijable
	public String id;
	public double radius=0.0;
	public String material="";//timber
	
	//defaultni konstruktor
	public RoundWindow() {
	}

	// specijalni konstruktor sa vrijednostima
	public RoundWindow(double iRadius) {
		radius = iRadius;
	}

	public RoundWindow(String iMaterial, double iRadius) {
		radius = iRadius;
		material = iMaterial;
	}
	
	public void calculateArea(){
		area=Math.PI*radius*radius;
	}

	@Override
	public String toString() {
		return "RoundWindow ima povrsinu od " + getArea();
	}
	
}
