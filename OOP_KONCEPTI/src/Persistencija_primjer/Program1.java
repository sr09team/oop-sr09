package Persistencija_primjer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class Program1 {

	public static void main(String[] args) {

		// nova instanca Window klase (rectangular)
		Window myWindow1 = new RectangularWindow(120, 100);
		myWindow1.calculateArea();
		System.out.println(myWindow1);

		// nova instanca Window klase (round)
		Window myWindow2 = new RoundWindow(100);
		myWindow2.calculateArea();
		System.out.println(myWindow2);

		Window deserializedWindow = null;
		// serijlizacija
		System.out.println("Serilizacija...");

		byte[] serilizedBytes = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;

		try {
			out = new ObjectOutputStream(bos);
			out.writeObject(myWindow1);
			serilizedBytes = bos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				try {
					out.close();
				} catch (IOException e) {
					// ignorisi close exception
				}
			try {
				bos.close();
			} catch (IOException e) {
				// ignorisi close exception
			}
		}
		// ...slanje drugom racunaru
		// deserilizacija
		System.out.println("Deserilizacija...");

		ByteArrayInputStream bis = new ByteArrayInputStream(serilizedBytes);
		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(bis);
			deserializedWindow = (Window) in.readObject();
			serilizedBytes = bos.toByteArray();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				bis.close();
			} catch (IOException e) {
				// ignorisi close exception
			}
			try {
				if (in != null)
					in.close();
			} catch (IOException e) {
				// ignorisi close exception
			}
		}
		
		// prikazi deserilized prozor
		System.out.println(deserializedWindow);
	}
}
