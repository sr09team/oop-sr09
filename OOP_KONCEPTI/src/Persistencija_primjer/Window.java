package Persistencija_primjer;

import java.io.Serializable;

public class Window implements Serializable {
	/**
	 * ID za serilized objekat
	 */
	private static final long serialVersionUID = 6580612868334292750L;
	//varijable
	public String id;
	public double width=0;
	public double height=0;
	public double area=0;
	public String material;
	public String materialList[] = {"Wood","Aluminium","Wood-Aluminium","Plastic"};
	
	//defaultni konstruktor
	public Window() {
	}
	
	//metoda izracunavanja povrsine
	public void calculateArea(){
		System.out.println("Ovo je calculate Area iz Window klase.");
	}
	
	//metoda koja vraca povrsinu
	public double getArea() {
		return area;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getMaterial() {
		return material;
	}
}
