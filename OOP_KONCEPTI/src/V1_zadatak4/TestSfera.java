package V1_zadatak4;

public class TestSfera {

	public static void main(String[] args) {

		// poziv statickog metoda getBrojac() klase Sfera kada smo izvan tijela te klase!!!
		System.out.println("Kreirano je " + Sfera.getBrojac() + "\n");

		// poziv podrazumljevanog konstruktora
		Sfera jedinicna=new Sfera();
		System.out.println("Jedinicna: "+ jedinicna);
		// poziv instancnog metoda zapremina() klase Sfera kada smo izvan tijela te klase!!!
		// instancni metod se uijvek poziva za konkretan objekat klase (ovdje: "jedinicna")
		System.out.println("Zapremina jedinicne je: " + jedinicna.zapremina());
		System.out.println("Kreirano je " + Sfera.getBrojac() + "\n");

		// poziv konstruktora koji prima 4 argumenta tipa double
		Sfera lopta = new Sfera(1,1,1,2);
		System.out.println("Lopta: " + lopta);
		System.out.println("Zapremina lopte je:" + lopta.zapremina());
		System.out.println("Kreirano je " + Sfera.getBrojac() + "\n");

		// poziv kopi-konstruktora
		Sfera kopijaLopte=new Sfera(lopta);
		System.out.println("Kopija lopte: "+ kopijaLopte);
		System.out.println("Kreirano je " + Sfera.getBrojac() + "\n");

		// objekat kreiran kopi-konstruktorom je identican originalu u trenutku
		// kreiranja. Nakon toga oni zive odvojene zivote: promjena jednog ne utice
		// na drugi
		System.out.println("Promjena poluprecnika kopije lopte na 4");
		kopijaLopte.setRadius(4);
		System.out.println("Kopija lopte: " + kopijaLopte);
		System.out.println("Lopta: " + lopta);
		System.out.println();

		System.out.println("Za istolopta nema poziva konstruktora!" +
		" Ne kreira se novi objekat!");

		Sfera istolopta=lopta;
		// Nema kreiranja objekta bez poziva konstruktora!!!
		// Dobili smo jos jednu referencu (istolopta) na postojeci objekat u memoriji
		// na koji smo imali referencu u promjenljivoj lopta
		System.out.println("Isto1opta: " + istolopta);
		System.out.println("Kreirano je "+ Sfera.getBrojac() + "\n");
		System.out.println("Promjena poluprecnika za istolopta na 7");
		istolopta.setRadius(7);
		System.out.println("Lopta: " + lopta);
		System.out.println("Istolopta: " + istolopta);
		System.out.println("Kopijalopte: " + kopijaLopte);
	}
}
