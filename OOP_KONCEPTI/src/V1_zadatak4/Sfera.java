package V1_zadatak4;

/* Klasa opisuje sfere u 3D */
public class Sfera {

	/* instancne promjenljive klase su x, y i 2 koordinata sfere
	i njen poluprecnik 	*/
	
	private double x, y, z, radius;
	/* staticka clanica je brojac kreiranih sfera */
	private static int brojac=0;

	/* podrazumljevani konstruktor - konstruktor bez argumenata
	kreira jedinicnu sferu sa centrom u koordinatnom pocetku */
	public Sfera(){
	brojac++;
	radius=1;
	/* x, y i z koordinata su automatski inicijalizovane na 0
	zbog svoje numericke prirode � double*/
	}

	/* konstruktor koji kreira jedinicnu sferu sa centrom u tacki cije su koordinate
	zadate */
	public Sfera(double x, double y, double z){
		this(); // poziv podrazumljevanog konstruktora - mora biti prva naredba! 
		this.x=x;
		this.y=y;
		this.z=z;
		// 2. varijanta: ako ne zovemo podrazumljevani konstruktor u prvoj naredbi, treba dodati i:
		// radius=1;
		//brojac++;
	}	

	public Sfera(double x, double y, double z, double r){
		this(x,y,z); // poziv prethodnog konstruktora - koji prime 3 argumenta
		radius=r;
	}

	/* kopi-konstruktor, kreira sferu identicnu postojecoj sferi s
	(i uvecava brojac kreiranih sfera)*/
	public Sfera(Sfera s){
		this(s.x,s.y,s.z,s.radius);
	}
	
	/* metod racuna zapreminu sfere, primjetiti koriscenje statickog clana PI klase Math */
	public double zapremina(){
		return 4./3*radius*radius*radius*Math.PI;
	}

	/* staticki metod get() za dohvatanje vrijednosti statickog clana brojac */
	public static int getBrojac(){
		return brojac;
	}

	/* metod set() za promjenu vrijednosti poluprecnika sfere */
	public void setRadius(double r){
		radius=r;
	}

	/* obicno se u tekstu zadatka kaze kako izgleda String-reprezentacija objekta
	koju metod treba da izgradi i vrati!!! */
	public String toString(){
		return "centar: (" + x + "," + y + "," + z + ") R=" + radius;
	}
}
