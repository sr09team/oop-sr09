/* program ilustruje citanje cijelih brojeva sa
standardnog ulaza i importovanje klasa koje ne pripadaju
 paketu java.lang
Korisnik unosi dva broja sa tastature, oni se saberu i
rezultat se ispise na standardni izlaz*/

package V1_zadatak1;
import java.util.Scanner;

public class Zbir {

	public static void main(String[] args) {
		// kreiranje objekta sc klase Scanner koji
		// ce sluziti za citanje sa standardnog ulaza
		// (kreiranje objekta se uvijek vrsi pozivom konstruktora
		// klase, tj. metoda koji se zove isto kao i klasa,
		// uz navodjenje kljucne rijeci NEW ispred)
		// System.in - standardni ulaz
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Unesite dva cijela broja:");
		// promjenljiva se moze uvesti tamo gdje nam je potrebna,
		// ne nuzno na pocetku bloka
		int a=sc.nextInt();
		int b = sc.nextInt();
		
		int zbir=a+b;
		System.out.println(a+" + "+b+" = "+zbir);
		sc.close();
	}

}
