package Niti_primjer;

public class MojThread extends Thread {
	public static int myCount=0;
	
	public void run(){
		while (MojThread.myCount <=10){
			try {
				System.out.println("Javlja se Thread: "+ (++MojThread.myCount));
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.out.println("Exception u threadu: " + e.getMessage());
			}
		}
	}
}
