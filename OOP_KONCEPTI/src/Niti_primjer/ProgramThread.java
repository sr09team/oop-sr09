package Niti_primjer;

public class ProgramThread {

	public static void main(String[] args) {
		System.out.println("Start glavnog Thread-a...");
		MojThread mst = new MojThread();
		mst.start();
		while (MojThread.myCount <=10){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.out.println("Exception u glavnom thread-u: " + e.getMessage());
			}
	}
	System.out.println("Kraj glavnog Thread-a...");
}
}
