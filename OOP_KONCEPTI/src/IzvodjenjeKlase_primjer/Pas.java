package IzvodjenjeKlase_primjer;

public class Pas extends Zivotnja {
	//konstruktori za objekat Pas
	public Pas(String sIme) {
		super("Pas"); 	//poziv baznog konstruktora
		ime=sIme;		//dato ime
		rasa="Nepoznata";	//podrazumevana vrijednost rase
	}
	
	public Pas(String sIme, String sRasa) {
		super("Pas");		//poziv baznog konstruktora
		ime = sIme;	//dato ime
		rasa = sRasa;	//DATA RASA
	}

	private String ime; //ime psa
	private String rasa; //rasa psa
	
	public String toString(){
		return super.toString() + "\n" + ime + " , " + rasa ;
		}
}
