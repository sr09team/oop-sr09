package IzvodjenjeKlase_primjer;

public class Zivotnja {
	private String vrsta;

	public Zivotnja(String vrsta) {
		this.vrsta = vrsta;
	}

	public String toString() {
		return "Ovo je " + vrsta;
	}
	
}
