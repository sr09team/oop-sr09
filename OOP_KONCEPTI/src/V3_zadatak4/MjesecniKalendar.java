package V3_zadatak4;

import java.util.*;

public class MjesecniKalendar {
	public static void main(String[] args) {
		
		Scanner tastatura= new Scanner(System.in);
		System.out.print("Uneste mjesec i godinu kalendara: ");
		int mjesec=tastatura.nextInt();
		int godina=tastatura.nextInt();
		tastatura.close();

		//kreiranje objekta klase GregorianCalendar iz java.util
		GregorianCalendar kalendar= new GregorianCalendar();
		int brojDanaM=0; //varijabla za broj dana u mjesecu
		//odredjivanje broja dana u mjesecu
		switch (mjesec){
			case 1: case 3: case 5: case 7: case 8: case 10 : case 12:
				brojDanaM=31;
				break;
			case 4: case 6: case 9: case 11:
				brojDanaM=30;
				break;
			case 2:
				// ispitivanje da li je godina prestupna
				if(kalendar.isLeapYear(godina))
					brojDanaM=29;
				else
					brojDanaM=28;
				break;
			default:
				System.out.println("Pogresno unesen mjesec");
				System.exit(-1);
		}

		// Kalendar pocinje od prvog dana mjeseca i godine
		kalendar.set(godina, mjesec -1, 1); // mjeseci pocinju od 0  

		int prviDan=kalendar.get(Calendar.DAY_OF_WEEK);
		System.out.println("  Pon  Uto  Sri  Cet  Pet  Sub  Ned");
		int k=0; // trenutna kolona prikaza kalendara
		// SUNDAY=1, MONDAY=2, ..., SATURDAY=7

		for (int i=Calendar.MONDAY; i<=Calendar.SATURDAY ; i++){
			if(prviDan==i) break;
			System.out.print("     ");
			k++;
		}

		for( int d=1; d<=brojDanaM; d++){
			System.out.printf("%5d", d);
			k++;
			if(k==7){
				System.out.println();
				k=0;
			}		
}
		System.out.println();
}
}
