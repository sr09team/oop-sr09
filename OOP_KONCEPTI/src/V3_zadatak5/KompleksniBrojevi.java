package V3_zadatak5;

public class KompleksniBrojevi {
private double r, i; // realni i imaginarni dio

//konstruktor
public KompleksniBrojevi(double r, double i){
	this.r=r;
	this.i=i;
}

// Get metodi
public double getR(){ 
	return r;
}
public double getI(){
	return i;
}



// moduo kompleksnog broja
public double moduo(){
	return Math.sqrt(r*r + i*i);
}

// Staticki metod za konjugovano kompleksni broj
// KompleksniBrojevi z = KompleksniBrojevi.konjugovanBroj(x)
public static KompleksniBrojevi konjugovanBroj(KompleksniBrojevi a){
	return new KompleksniBrojevi(a.r, -a.r);
	}

// Objektni metod za kkonjugovano kompleksni broj
// KompleksniBrojevi z = x.konjugovanBrojevi();
public KompleksniBrojevi konjugovanBroj(){
	return new KompleksniBrojevi(this.r, -this.i);
}

// Staticki metod za sabiranje dva kompleksna broja
// KompleksniBrojevi z = KompleksniBrojevi.zbir(x, y)
public static KompleksniBrojevi zbir(KompleksniBrojevi a, KompleksniBrojevi b){
	return new KompleksniBrojevi(a.r+b.r, a.i + b.i);
}

//Objektni metod za sabiranje dva kompleksna broja
// KompleksniBrojevi z = x.dodaj(y);
public KompleksniBrojevi dodaj (KompleksniBrojevi a){
	return new KompleksniBrojevi(this.r + a.r, this.i + a.i);
}

//Staticki metod za proizvod dva kompleksna broja
// KompleksniBrojevi z = KompleksniBrojevi.proizvod(x, y)
public static KompleksniBrojevi proizvod(KompleksniBrojevi a, KompleksniBrojevi b){
	return new KompleksniBrojevi(a.r*b.r - a.i*b.i, a.r * b.i+b.r*a.i);
}

//Objektni metod za proizvod dva kompleksna broja
// KompleksniBrojevi z = x.pomnozi(y);
public KompleksniBrojevi pomnozi(KompleksniBrojevi a){
	return new KompleksniBrojevi(a.r*this.r - a.i*this.i, a.r * this.i+this.r*a.i);
}

public String toString(){
	return "[" + r + ","+ i + "]";
}

// glavni program - kreirani objekti x, y tipa klase KompleksniBrojevi
public static void main(String[] args) {
	KompleksniBrojevi x= new KompleksniBrojevi (1, 1);
	System.out.println("x=" +x);
	System.out.println("Re x= " +x.getR());
	System.out.println("Im x= " +x.getI());
	System.out.println("Moduo x = " +x.moduo());
	System.out.println("Konjugovano x= " +x.konjugovanBroj());
	System.out.println("x + konjugovano x = " + KompleksniBrojevi.zbir(x, x.konjugovanBroj()));

	KompleksniBrojevi y= new KompleksniBrojevi (2, 0);
	System.out.println("x+y = " +x.dodaj(y));

	System.out.println("x*y = +x.pomnozi(y)");
}
}

