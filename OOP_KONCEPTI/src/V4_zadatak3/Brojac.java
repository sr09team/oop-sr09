package V4_zadatak3;

public class Brojac {
	private int broj; //pocetna vrijednost je 0
	
	public void uvecaj(){
		broj++;
	}
	
	public void reset(){
		broj=0;
	}
	
	public int getBroj(){
		return broj;
	}
}
