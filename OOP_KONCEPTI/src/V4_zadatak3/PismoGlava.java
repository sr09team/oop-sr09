package V4_zadatak3;

import java.util.Scanner;

public class PismoGlava {

	public static void main(String[] args) {

		int brojBacanja; // broj bacanja novcica
		Brojac brojacPisama = new Brojac(); // koliko puta je palo pismo
		Brojac brojacGlava = new Brojac(); // koliko puta je pala glava
		Scanner tastatura = new Scanner(System.in);
		System.out.print("Unesite broj bacanja novcica: ");
		brojBacanja = tastatura.nextInt();
		while (brojBacanja > 0) {
			// buduci da se vrsi ponavljpanje programa i po zavrsetku potrebno
			// je resetovati brojace pri novom odabranom broju bacanja
			brojacPisama.reset();
			brojacGlava.reset();

			for (int i = 0; i < brojBacanja; i++)
				// vjerovatnoca pisma i glave je ista i iznosi 0.5
				if (Math.random() < 0.5)
					brojacPisama.uvecaj();
				else
					brojacGlava.uvecaj();

			System.out.println("U " + brojBacanja + " bacanja, palo je ");
			System.out.println(brojacPisama.getBroj() + " pisama.");
			System.out.println("U " + brojBacanja + " bacanja, palo je ");
			System.out.println(brojacGlava.getBroj() + " glava.");
			System.out.println();

			System.out.print("Unesite broj bacanja novcica: ");
			brojBacanja = tastatura.nextInt();
		}
		tastatura.close();
	}

}
