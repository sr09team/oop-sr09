/* Test-klasa za rad sa tackama */
package V2_zadatak1;

public class TestTacke {

	public static void main(String[] args) {

		/* pozivom podrazumljevanog konstruktora
		kreiramo koordinatni pocetak */
		Tacka kp = new Tacka();
		
		/* kada ispisujemo kreiranu tacku implicitno biva pozvan metod toString()
		klase Tacka koji vraca String-reprezentaciju tacke u obliku na koji smo 
		navikli u matematici: (x, y) */
		System.out.println("kp = " + kp);

		/* kreiramo tacku sa koordinatama 3 i 4 */
		Tacka t = new Tacka(3, 4);

		/* ispisujemo je */
		System.out.println("t = " + t);

		/* pozivom kopi�konstruktora kreiramo kopiju postojece tacke t */
		Tacka kopijaT = new Tacka(t);
		System.out.println("kopijaT = " + kopijaT);

		/* odredjujemo i ispisujemo rastojanje od tacke kp do tacke t poziva se nestaticki
		 * metod rastojanje() klase Tacka za konkretan objekat kp (kp je "tekuci" objekat)*/
		System.out.println("rastojanje od kp do t je "+ kp.rastojanje(t));

		/* vrsimo pomjeranje tacke za -3 pa x-osi (i 0 po y-osi) */
		t.pomjeri(-3, 0);
		
		System.out.println("Pomjeranje tacke t za -3 p0 x (1 0 p0 y)");
		/* ispisujemo tacku nakon pomjeranja */
		System.out.println("t = " + t);
		/* ponovo racunamo i ispisujemo rastojanje izmedju kp i t
		* samo sto nam je sada tekuca tacka t (rastojanje je metrika, 
		* vazi svojstvo simetrije, pa je svejedno koja tacka je tekuca, 
		* a koja se prosljedjuje kao argument metoda) */
		System.out.println("rastojanje od kp do t je "+ t.rastojanje(kp));
	}
}
