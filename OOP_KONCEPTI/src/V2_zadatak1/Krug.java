package V2_zadatak1;

/* klasa Krug je izvedena iz klase Povrs:
 * kljucna rijec EXTENDS za kojom slijedi ime bazne klase! */

public class Krug extends Povrs {

	/* pored svega sto ima Povrs (centralne tacke) krug ima jos i: */
	private double radius; // poluprecnik

	/* objekat izvedene klase je prosirenje objekta bazne,
	tj. ima sve sto ima bazna (sadrzi u sebi citav podobjekat
	bazne klase), i jos sve sto je definisano u	ovoj klasi, pa 
	je u konstruktoru potrebno izvrsiti	inicijalizaciju svih 
	tih clanova (u ovom konkretnom slucaju:	i centralne tacke 
	(bazni dio) i poluprecnika (dio specifican za ovu potklasu) */

	public Krug(Tacka centar, double radius){
	/* inicijalizacija baznog dijela objekta vrsi se pozivom 
	 * konstruktora bazne klase pomocu kljucne rijeci super() 
	 * kao imena metoda i to mora biti prva linija u tijelu 
	 * konstruktora izvedene, tj. ove klase. Ako se to izostavi,
	 *  kompajler implicitno pokusava da pozove podrazumljevani 
	 *  konstruktor bazne klase: super();
	 *  a ukoliko njega nema, dobija se poruka o gresci	*/
		super(centar);
	/* inicijalizacija clanova specificnih za ovu potklasu
	* vrsi se na uobicajeni nacin:*/
		this.radius = radius;
	}

	/* kopi-konstruktor, kreira krug identican postojecem krugu k */
	public Krug(Krug k){
	// inicijalizacija baznog dijela objekta:
	
	//1. varijanta, kompaktna, uvijek izgleda ovako:
	// poziva se kopi-konstruktor bazne klase (koji mora da postoji)
	// i prosljedi mu se kao argument postojeci objekat
	// izvedene, ciju kopiju pravimo.
	// Kopi-konstruktor bazne klase je metod definisan tako da
	// mu je argument tipa bazne klase (Povrs),
	// a u tom slucaju dopusteno je da se kao stvarni
	// argument, prilikom poziva metoda, prosljedi objekat tipa Povrs,
	// ali i objekat tipa proizvoljne potklase, pa moze i Krug
	// (krug JE povrs, jer je klasa Krug izvedena iz klase Povrs)
	super(k);

	// 2. varijanta
	// "ruznija", neophodno je da u baznoj klasi postoje get*()-metodi za
	// svaki od atributa te klase, umjesto kopi—konstruktora bazne klase,
	// ovdje se poziva konstruktor koji prima sve neophodne podatke, a oni
	// se dobijaju pozivom odgovarajucih get*()—metoda za postojeci objekat
	// k kao tekuci (ovdje bazna klasa ima samo jedan atribut, pa to i ne 
	// dijeluje tako strasno, ali ovo moze da bude i prilicno glomazan izraz)
	// get*()-metode je moguce pozvati za objekat izvedene klase Krug
	// jer bivaju naslijedeni (zbog pristupnog atributa public) super(k.getCentar());
	radius = k.radius;
	}
	
	/* zakomentarisati ovaj metod, pa pokrenuti program
	bice pozvan nasljedjeni metod toString() bazne klase Povrs.
	mi ovde vrsimo predefinisanje (overriding) nasljedjenog metoda
	toString() bazne klase, a kako on ima isti potpis kao i ovaj,
	da bismo pozvali njega, moramo koristiti kljucnu rec super */
	
	public String toString(){
		return "krug, " + super.toString() +", poluprecnik je " + radius;
	}
}
