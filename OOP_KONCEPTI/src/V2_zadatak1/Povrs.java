package V2_zadatak1;

/* definicija bazne klase Povrs*/

/* iz ove klase cemo izvesti najprije Krug,
	a potom i Pravougaonik, a iz njega Kvadrat*/

/* iz same definicije bazne klase ni po cemu se ne vidi
	da ona dalje ucestvuje u postupku nasledjivanja niti koje 
	su klase izvedene iz nje*/

/* sama klasa Povrs ima univerzalnu superklasu Object kao svoju
 * baznu klasu (jer nije eksplicitno navedeno, koriscenjem
 * kljucne rijeci EXTENDS, da je izvedena iz neke druge klase)*/

public class Povrs {

	/* svaka povrs ima centralnu tacku,
	* kojom je odredjena njena pozicija u ravni	*/
	
	private Tacka centar; // NE NASLJEDJUJE SE: jer je private

	/* u ovoj klasi nije definisan podrazumjevani konstruktor
	* sto uzrokuje da, tokom pisanja izvedenih klasa,
	* Eclipse prikazuje poruke o greskama sve dok se iz
	* svakog konstruktora izvedene klase eksplicitno ne pozove
	* neki postojeci konstruktor bazne, tj. ove klase */

	// KONSTRUKTORI SE NIKADA NE NASLJEDJUJU!

	/* konstruktor koji prima sve neophodne podatke */
	public Povrs(Tacka centar){
	// Moguce su dve varijante:

	// 1. varijanta: "samo" se uzme postojeca referenca.
	// Nakon promjene tacke koja je koristena za kreiranje
	// povrsi, mijenja se i centralna tacka povrsi,
	// jer je to zapravo isti objekat, samo imamo dvije
	// reference (this.centar i centar) na njega.

	//this.centar = centar;
		
	// 2. varijanta: kreira se kopija tacke argumenta,
	// pa izmjene tacke koja je prosljedjena kao argument,
	// ne uzrokuju izmjenu centralne tacke povrsi
	this.centar = new Tacka(centar);
	}

	/* kopi-konstruktor, kreira povrs identicnu postojecoj povrsi p */
	public Povrs(Povrs p){
		/* poziv gornjeg konstruktora - prosljedimo mu centralnu tacku povrsi p */
		this(p.centar);
	}

	// METODI getCentar() i toString() SU DEFINISANI SA
	// PRISTUPNIM ATRIBUTOM public
	// PA CE BITI NASLJEDJENI U IZVEDENIM KLASAMA

	/* u ovom primjeru ovaj metod ce nam trebati jedino ako zelimo da napisemo
	* kopi-konstruktor izvedene klase Krug na "tezi" nacin:	*/

	public Tacka getCentar(){
		return centar;
	}

	/* String-reprezentacija povrsi ukljucuje informaciju o centralnoj tacki */
	public String toString(){
		return "centar je u tacki " + centar;
	}
}
