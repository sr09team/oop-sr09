package V2_zadatak1;

public class Tacka {
	

	/* atributi klase su koordinate tacke */
	private double x, y;

	/* podrazumljevani konstruktor (konstruktor bez argumenata)
	* kreira tacku u koordinatnom pocetku (x=0, y=0)*/

	public Tacka(){
	}

	/* kreiranje tacke od svih potrebnih argumenata */
	public Tacka(double x, double y){
	this.x = x;
	this.y = y;
	}

	/* kopi-konstruktor, kreira se tacka identicna postojecoj tacki t */

	/* kljucna rijec final znaci da metod nece mijenjati argument t
	tj. nece se nigdje u tijelu metoda pojaviti t=...
	sto nece sprijeciti da se mijenjaju clanovi objekta na koji
	t referise (npr. moglo bi t.x=...)	*/
	
	public Tacka(final Tacka t){
		/* poziv gornjeg konstruktora,
		kreirace tacku cija je x-koordinata jednaka x-koordinati tacke t
		a y-koordinata jednaka y-koordinati tacke t	*/
		this(t.x, t.y);
	}

	/* metod racuna i vraca rastojanje od tekuce do zadate tacke t */
	public double rastojanje(final Tacka t){
		/* sqrt() je staticki metod klase Math */
		return Math.sqrt((x-t.x)*(x-t.x)+(y-t.y)*(y-t.y));
	}

	/* metod vrsi pomjeranje tacke za pomjeraj deltaX po x-osi
	i pomjeraj deltaY po y-osi */
	public void pomjeri(double deltaX, double de1taY){
		x+=deltaX;
		y+=de1taY;
	}

	/* metod predefinise nasljedjeni metod toString() klase Object
	i vraca uobicajenu String-reprezentaciju tacke */
	public String toString(){
		return "("+x+", "+y+")";
	}
}
