package V5_zadatak1;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class PisiFajl {

	public static void main(String[] args) {

		/* kreiranje objekta koji sluzi za upis u tekstualni fajl */
		/* print(), println() metodi, prihvattaju argument primitivnog tipa
		 * char[], String, i Object tipa. Ispisuje se String-reprezentacija argumenta.
		 * kao za System.in objekat.
		 * Ako ne postoji fajl sa datim imenom, bice kreiran novi,
		 * u suprotnom se ono sto pisemo prepisuje preko starog sadrzaja!
		 */
		/* koristi se TRY-WITH-RESOURCES */
		try(PrintWriter izl = new PrintWriter(new FileWriter ("izlaz.txt"))){
			for(int i=0;i<10;i++) izl.println((i+1)+". red");
			/* posto se vrsi baferovanje, ako se zeli forsiranje ispisa,
			 * moze se pozvati metod FLUSH()
			 */
			izl.flush();
			
			/* slicno formatiranju izlaza u C-u */
			izl.println("Formatirani ispis: ");
			double koeficjent_a=5.4;
			izl.printf("Koeficjent_a = %12.6f",koeficjent_a );
			izl.println();
			/* probati umjesto gornja dva reda i
			 * izl.printf("Koeficjent_a = %.6f\r\n",koeficjent_a );
			 */
		}catch (IOException ex){
			System.out.println("Neuspjela operacija pripreme za upis u fajl izlaz.txt");
			System.exit(1);
		}
		
	}

}
