package V5_zadatak7;

import java.io.Serializable;
import java.util.Scanner;

public class Unos implements Comparable<Unos>,Serializable{
private static final long serialVersionUID=1L;
private Osoba osoba;
private Broj broj;

public Unos(Osoba osoba, Broj broj) {
	this.osoba = osoba;
	this.broj = broj;
}

public Osoba getOsoba() {
	return osoba;
}

public Broj getBroj() {
	return broj;
}
public int compareTo(Unos u){
	return osoba.compareTo(u.osoba);
}

@Override
public String toString() {
	return osoba + ", broj telefona je: " + broj;
}
public static Unos ucitajUnos(Scanner sc){
	return new Unos(Osoba.ucitajOsobu(sc),Broj.ucitajBroj(sc));
}




}
