package V5_zadatak7;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Imenik imenik = new Imenik();

		/*
		 * standardni nacin za rad sa opcijama: beskonacna petlja koja se
		 * naredbom return prekida kada korisnik izabere opciju za kraj (opcija
		 * 9)
		 */

		for (;;) {
			System.out.println("Unesite");
			System.out.println("\t1 za novi unos");
			System.out.println("\t2 za pretragu");
			System.out.println("\t3 za listanje leksikografski");
			System.out.println("\t9 za kraj");

			try {
				int izbor = sc.nextInt();
				switch (izbor) {
				case 1:
					imenik.dodajUnos(Unos.ucitajUnos(sc));
					break;
				case 2:
					Osoba o = Osoba.ucitajOsobu(sc);
					Unos u = imenik.getUnos(o);
					if (u == null)
						System.out.println("Osoba " + o + " nije u imeniku");
					else
						System.out.println("Broj telefona osobe " + o + " je " + u.getBroj());
					break;
				case 3:
					imenik.listaj();
					break;
				case 9:
					imenik.sacuvaj();
					System.out.println("Kraj");
					break;
				default:
					System.out.println("Pogresna opcija, pokusajte ponovo.");
					break;
				}
			} catch (InputMismatchException e) {
				// token koji je uzrokovao izbacivanje izuzetka ostaje
				// na ulazu, pa ga je neophodno procitati kako bi se moglo
				// nastaviti dalje
				String greska = sc.next();
				System.out.println("\"" + greska + "\" je pogresan izbor, pokusajte ponovo");
				continue;
			}

		}

	}

}
