package V5_zadatak7;

import java.io.Serializable;
import java.util.Scanner;

public class Broj implements Serializable{
	private static final long serialVersionUID=1L;
	private String pozivni, lokal;
	
	public Broj(String pozivni, String lokal) {
		this.pozivni = pozivni;
		this.lokal = lokal;
	}

	@Override
	public String toString() {
		return "("+pozivni+")"+lokal;
	}
	
	public static Broj ucitajBroj(Scanner sc){
		System.out.println("Unesite pozivni i broj u lokalu:");
		return new Broj(sc.next(),sc.next());
	}
	
	
	

}
