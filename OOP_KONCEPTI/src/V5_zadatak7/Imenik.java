package V5_zadatak7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;

public class Imenik {
private HashMap <Osoba,Unos> imenik = new HashMap<Osoba,Unos>();
private File datoteka = new File("C:\\Beg Java Stuff\\Imenik.bin");

/* konstruktor radi na sledeci nacin:
 * ako se program pokrece prvi put fajl ne postoji pa ce vektor osobe biti prazan
 * ako je program vec pokretan, neposredno prije zavrsetka pozvan je metod sacuvaj 
 * koji je u fajl, koristeci serilizaciju, upisao tekuci sadrzaj vektora osobe. Fajl,
 *  dakle postoji, te se iz njega iscitava rezultat prethodnih izvrsavanja
 */

public Imenik(){
	if(datoteka.exists()){
		ObjectInputStream in = null;
		
		try {
			in=new ObjectInputStream(new FileInputStream(datoteka));
			/* kastovanje je neophodno, kako u vrijeme kompajliranja ni na koji nacin nije moguce
			 * utvrditi da li se kastuje u stvarnu klasu objekta koji se cita iz fajla, upozorenje 
			 * kompajlera(zuti uzvicnik i vijugava linija) je neizbjezno
			 */
			HashMap<Osoba,Unos> readObject = (HashMap<Osoba,Unos>)in.readObject();
			imenik=readObject;
		} catch (IOException e) { //alternativa u Java 7: multi-catch
			e.printStackTrace();
			System.exit(1);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} finally {	// zelimo da se tok zatvori bez obzira da li je doslo do izbacivanja izuzetka ili ne
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
		
	}
}

/* metod koji se poziva kada korisnik izabere opciju 1 */
public void dodajUnos(Unos u){
	imenik.put(u.getOsoba(),u);
}

/* metod koji se poziva kada korisnik izabere opciju 2 */
// vrijednost je null ukoliko o nije koristen kao kljuc za smjestanje unmosa u mapu
public Unos getUnos(Osoba o){
	return imenik.get(o);
}

/* metod koji se poziva kada korisnik izabere opciju 3 */
public void listaj(){
	/* metod values() vraca kolekciju referenci na objekte iz mape
	 * povratni tip metoda je Collection<>
	 * (Collection<> je interfejs, iz njega je izveden List<>, a njega
	 *  implementiraju klase: Vector<>, Stack<>, ArrayList<>, LinkedList<>)
	 *  kreiramo odgovarajucu povezanu listu, koju potom sortiramo
	 *  i ispisujemo njene elemente 
	 */
	LinkedList<Unos> unosi = new LinkedList<Unos>(imenik.values());
	Collections.sort(unosi); //argument je tipa list
	for(Unos u:unosi){
		System.out.println(u);
	}
}

/* metod koji se zove neposredno prije zavrsetka izvrsavanja programa kako
 * bi u fajl upisao tekuci sadrzaj vektora (tom prilikom koristi se serilizacija)
 */
public void sacuvaj(){
	ObjectOutputStream out = null;
	
	try {
		System.out.println("Upis imenika u file");
		out=new ObjectOutputStream(new FileOutputStream(datoteka));
		out.writeObject(imenik);
		System.out.println("Gotovo");
	} catch (IOException e) {
		e.printStackTrace();
		System.exit(1);
	} finally {		// u Java 7 moze se koristiti try-with-resources
		if(out!=null)
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
	}
}
}
