package V5_zadatak7;

import java.io.Serializable;
import java.util.Scanner;

public class Osoba implements Comparable<Osoba>, Serializable {
	private static final long serialVersionUID = 1L;
	// o gornjoj konstanti mozete procitati iz PDF-a okacenog uz primjer
	// upozorenje uz prvu liniju definicije klase se odnosi na nju
	private String ime;
	private String prezime;

	public Osoba(String ime, String prezime) {
		this.ime = ime;
		this.prezime = prezime;
	}

	public int compareTo(Osoba o) {
		int rez = prezime.compareTo(o.prezime);
		if (rez == 0)
			return ime.compareTo(o.ime);
		return rez;
	}

	// objekti klase Osoba ce biti koristeni kao kljucevi u has map-i
	// te je potrebno predefinisati metode equals() i hashCode();
	public boolean equals(Object o) {
		if (!(o instanceof Osoba))
			return false;
		return compareTo((Osoba) o) == 0;
	}

	/*
	 * koristi se linearna kombinacija hash-kodova atributa (oba su tipa String,
	 * pa se za njih zove metod hashCode() klase String) koeficjetni u linearnoj
	 * kombinaciji su razliciti prosti brojevi (ovdje 7 i 13)
	 */
	public int hashCode() {
		return 7 * ime.hashCode() + 13 * prezime.hashCode();
	}

	@Override
	public String toString() {
		return prezime + " " + ime;
	}

	// staticki metod za ucitavanje objekta ove klase
	// koristenjem objekta sc klase Scanner
	public static Osoba ucitajOsobu(Scanner sc) {
		System.out.print("Unesite ime i prezime osobe: ");
		return new Osoba(sc.next(), sc.next());
	}
}
