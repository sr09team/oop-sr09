package V2_zadatak3;

//nemamo vise baznu klasu Povrs
//vec klase Krug, Pravougaonik i iz nje izveden Kvadrat
//koje implementiraju interfejse Povrsina, Obim i Opisivanje

public class Krug implements Povrsina, Obim, Opisivanje{

	//svaki krug ima svoj centar i poluprecnik
	private Tacka centar;
	private double radius;

	//konstruktor za kreiranje Kruga od svih neophodnih podataka
	public Krug(Tacka centar, double radius){
		this.centar = new Tacka(centar);
		this.radius = radius;
		}

	//kopi-konstruktor
	public Krug(final Krug k){
		this(k.centar, k.radius);
		}

	//implementacija polimorfnog metoda
	//koji ima isti potpis kao i metod u interfejsu
	//isti povratni tip kao metod u interfejsu
	//i isti pristupni atribut kao metod u interfejsu

	public double povrsina(){
		//PI je staticki clan klase Math iz paketa java.lang
		return radius*radius*Math.PI;
	}

	public double obim(){
		return 2*radius*Math.PI;
	}

	/* predefinisanje metoda opisi iz interfejsa Opisivanje
	 * metod ima isti potpis, public je dok mu je povratni 
	 * tip potklasa povratnog tipa koji metod ima u interfejsu (kovarijantan) */
	public Kvadrat opisi(){
		//kvadrat opisan oko kruga ima isti centar
		//i stranicu dva puta duzu od poluprecnika kruga
		return new Kvadrat(centar, 2*radius);
		}
	
	//String-reprezentacija kruga
	public String toString(){
		return "krug centar je u tacki" + centar + " poluprecnik je "+ radius;
	}
}





