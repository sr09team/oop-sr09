package V2_zadatak3;

public interface Povrsina {
	/* za metode u interfejsu podrazumjevaju se kljucne rijeci
	 * public i abstract */
	double povrsina();
}
