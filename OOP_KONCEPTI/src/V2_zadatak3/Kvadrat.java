package V2_zadatak3;

//klasa izvedena iz Pravougaonik, a ova iz Povrs
public class Kvadrat extends Pravougaonik{
	//Kvadrat je pravougaonik kod koga je a==b
	//klasa nema dodatnih atributa

	//konstruktor kreira kvadrat na osnovu svih neophodnih podataka
	public Kvadrat(Tacka centar, double a){
		super(centar, a, a);
	}

	//String-reprezentacija kvadrata
	public String toString(){
		//getCentar() je metod koji je public u klasi Povrs,
		//nasljedjen u Pravougaonik, a iz nje i u Kvadrat
		//getA() je nasledjen od Pravougaonik
		return "kvadrat centar u tacki " + getCentar() + " a=" + getA();
	}
}

