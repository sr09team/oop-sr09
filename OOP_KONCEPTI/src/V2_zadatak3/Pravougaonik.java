package V2_zadatak3;

public class Pravougaonik implements Povrsina, Obim, Opisivanje{
	// pored centralne tacke (bazni dio objekta)
	// svaki pravougaonik ima jos svoju sirinu i visinu
	private Tacka centar;
	private double a, b; // a-sirina, b-visina

	// kreiranje pravougaonika od svih neophodnih podataka
	public Pravougaonik(Tacka centar, double a, double b){
		this.centar = new Tacka(centar);
		this.a=a;
		this.b=b;
		}

	// kopi-konstruktor
	public Pravougaonik(final Pravougaonik p){
		this(p.centar,p.a,p.b);
		}

	// racunanje dijagonale tekuceg pravougaonika kao duzine hipotenuze 
	// pravouglog trougla sa katetama a i b
	public double dijagonala(){
		return Math.sqrt(a*a+b*b);
		}

	// metod racuna i vraca krug opisan oko tekuceg pravougaonika
	public Krug opisaniKrug(){
		// krug ima istu centralnu tacku kao i pravougaonik 
		return new Krug(centar, 0.5*dijagonala());
		}
	public Krug opisi(){
		//metod opisaniKrug() vec radi sta treba, pa ga samo pozovemo
		return opisaniKrug();
	}
	
	// metod koji ce biti nasljedjen u izvedenoj klasi Kvadrat
	public double getA(){
		return a;
	}
	
	
	public Tacka getCentar() {
		return centar;
	}

	// implementacija polimorfnog metoda (metod ima: isti potpis, isti povratni tip
	// i isti pristupni atribut kao i u baznoj klasi)racunanje povrsine tekuceg pravougaonika
	public double povrsina(){
		return a*b;
	}

	public double obim(){
		return 2*(a+b);
	}

	// Stringóreprezentacija pravougaonika
	public String toString(){
		return "pravougaonik centar je u tacki" + centar +" a= "+ a + " b= "+b;
	}
}
	


