package V2_zadatak3;

//pomocna tacka (centar povrsi bice objekat ove klase)

public class Tacka {
	private double x, y; // tacka je u 2D odredjena
						//svojom x i y koordinatom
	
	/* podrazumljevani konstruktor, kreira se tacka
	 *  u koordinatnom pocetku */
	
	public Tacka(){
	}
	/* kreiranje tacke od svih neophodnih podataka
	 * (zadate su obje koordinate) */
	
	public Tacka(double x, double y){
		this.x = x;
		this.y = y;
		}
	/* kopi-konstruktor, kreiranje tacke identicne
	 * postojecoj tacki t */
	public Tacka(final Tacka t){
		this(t.x, t.y);
		}
	/* metod racuna i vraca rastojanje od tekuce do tacke t
	 * po poznatim formulama */

	public double rastojanje(Tacka t){
		/* sqrt() je staticki metod klase Math iz paketa java.lang
		 * sluzi za racunanje kvadratnog korjena */
		return Math.sqrt((x-t.x)*(x-t.x)+(y-t.y)*(y-t.y));
		}
	
	/* metod pomjera tacku za deltaX po x-osi i deltaY po y-osi */
	public void pomjeri(double deltaX, double deltaY){
		x+=deltaX;
		y+=deltaY;
		}
	
	/* String-reprezentacija tacke */
	public String toString(){
		return "("+x+", "+y+")";
		}
}