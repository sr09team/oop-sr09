package V2_zadatak3;

/*primjer ilustruje upotrebu interfejsa za polimorfno pozivanje metoda
nije moguce kreirati objekat tipa interfejsa ali je moguce deklarisati 
promjenljivu tipa interfejsa u kojoj se moze cuvati referenca na objekat
proizvoljne klase koja implementira taj interfejs
ova promjenljiva se moze koristiti za polimorfno pozivanje metoda iz interfejsa */

import java.util.Random;

public class TestPolimorfizam {
	public static void main(String[] args) {

		//da bi elementi bili tipa Krug,
		// Pravougaonik, Kvadrat, niz je tipa interfejsa 
		// koji implementiraju sve te klase
		Povrsina povrsi[]={new Krug(new Tacka(),1),
				new Krug(new Tacka(1,1),2),
				new Pravougaonik(new Tacka(),3,4),
				new Kvadrat(new Tacka(4,6), 2)};

		// objekat klase Random iz paketa java.uti1
		// predstavlja generator pseudoslucajnih brojeva
		Random izbor = new Random();

		
		// 5 puta se slucajno bira element niza povrsi
		for(int i=0; i<5; i++){
			// p je promjenljiva tipa bazne klase koja cuva
			// referencu na objekat izvedene (jedan od uslova za polimorfizam)
			// izbor.nextInt(n) vraca slucajan cio broj iz opsega [0,...,n-1]
			// sto je validan indeks niza koji ima n elemenata
			// da bismo slucajno izabrali element niza povrsi,
			// nadjemo slucajan indeks za taj niz:
			// izbor.nextInt(povrsi.length)
			// a indeksiranjem niza tim indeksom dobijamo slucajan element niza:
			Povrsina p = povrsi[izbor.nextInt(povrsi.length)];
			System.out.println(p); // polimorfno se poziva metod toString()
				// koji takodje ispunjava uslove za polimorfizam
			
			// polimorfno pozivanje metoda povrsina()
			System.out.println("Povrsina je " + p.povrsina());
			// da bi smo polimorfno pozivali metod obim()
			//moramo izvrsiti kastovanje reference u tip Obim
			System.out.println("Obim je " + ((Obim)p).obim());
			
			//Polimorfno pozivanje metoda za vracanje opicanog objekta
			//(iz istih razloga kao i maloprije, i ovdje je neophodno kastovanje)

				System.out.println("0pisani "+((Opisivanje)p).opisi());
			System.out.println();
		}
	}
}

