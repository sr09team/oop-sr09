package Interfejs_primjer;

public class RectangularWindow extends Window {
	public String id;
	public double width=0.0;
	public double height=0.0;
	public String material="";//timber
	
	//defaultni konstruktor
	public RectangularWindow() {
	}

	// specijalni konstruktor sa vrijednostima
	public RectangularWindow(double iWidth, double iHeight) {
		width = iWidth;
		height = iHeight;
	}

	public RectangularWindow(String iMaterial, double iWidth, double iHeight) {
		width = iWidth;
		height = iHeight;
		material = iMaterial;
	}
	
	public void calculateArea(){
		area=width*height;
	}
}
