package Interfejs_primjer;

public interface OpeningConstants {
	
	//koeficjent gubitka toplote
	public static final double Uw=0.8;
	
	//lista materijala
	public static final String materialList[] = {"Wood","Aluminium","Wood-Aluminium","Plastic"};

}
