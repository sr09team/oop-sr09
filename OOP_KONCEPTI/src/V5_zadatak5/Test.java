package V5_zadatak5;

import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Vector;

public class Test {

	public static Scanner sc=new Scanner(System.in);
	public static void main(String[] args) {
		
		// osobe koje se prijavljuju na kasting smjestaju se u vektor osoba "cast"
		Vector <Osoba> cast = new Vector<Osoba>();
		Osoba neko=null;
		for(;;){
			neko=ucitajOsobu();
			// kada nema vise osoba izlazi se iz beskonacne petlje
			if(neko==null) break;
			//ako je ucitanak osoba ona se dodaje na kraj vektora
			cast.add(neko);
		}
		
		// koliko je osoba u vektoru
		int KolikoImaOsoba=cast.size();
		System.out.println("Prijavljeno je "+ KolikoImaOsoba);
		
		// prvi nacin prolaska kroz vektor, koristenjem iteratora
		System.out.println("Iterator");
		Iterator<Osoba> iter= cast.iterator();
		while (iter.hasNext()){
			System.out.println(iter.next());
		}
		
		System.out.println("get");
		
		// drugi nacin prolaska kroz vektor "indeksiranjem" pomocu
		// metode get() ( vraca element vektora koji se nalazi na zadatoj poziciji - i pozicije se broje od 0)
		for(int i=0;i<cast.size();i++){
			System.out.println(cast.get(i));
		}
		
		// sortiranje vektora ( moguce je jer klasa Osoba, ciji objekti su elementi vektora, implementira
		// Comparable <> interfejs
		Collections.sort(cast);
		
		for(int i=0;i<cast.size();i++){
			System.out.println(cast.get(i));
		}
		
		Osoba traziSe=ucitajOsobu();
		// da bi se contains() ponasao na ocekivani nacin klasa objekta koji se cuvaju u kolekciji (Osoba)
		// mora imati adekvatno predefinisan metod equals() koji nasljedjuje od univerzalne superklase Object
		
		if(cast.contains(traziSe)) System.out.println("Osoba "+ traziSe+" se prijavila na kasting");
		else System.out.println("Osoba "+ traziSe+" se NIJE prijavila na kasting");
	}
	
	// pomocni metod za ucitavanje Osobe sa standardnog ulaza
	// ako se umjesto imena unese "!" metod vraca vrijednost null kao oznaku da nema vise osoba
	private static Osoba ucitajOsobu() {
		// TODO Auto-generated method stub
		System.out.print("Unesite ime osobe ili ! za kraj: ");
		String ime=sc.next();
		if(ime.equals("!"))	return null;
		System.out.print("Unesite prezime osobe: ");
		return new Osoba(ime,sc.next());
	}

}
