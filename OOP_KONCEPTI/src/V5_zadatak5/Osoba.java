package V5_zadatak5;

public class Osoba implements Comparable<Osoba>{
private String ime;
private String prezime;

public Osoba(String ime, String prezime) {
	this.ime = ime;
	this.prezime = prezime;
}

/* implementacija metoda compareTo iz interfejsa Comparable<>
 * (ne zaboravi "implements Comparable<Osoba>" u prvoj liniji definicije klase!)
 * omogucuje da se metodom Collections.sort() sortira kolekciju objekata tipa Osoba
 * metod treba da vrati int:
 * 	0 ako se radi o jednakim objektima
 * <0 ako je tekuci objekat "manji" od objekta o
 * >0 ako je tekuci objekat "veci" od objekta o
 */
public int compareTo(Osoba o){
	// najprije se porede prezime tekuce i osobe o
	int rez= prezime.compareTo(o.prezime); // ukoliko su prezimena ista, vraca se 0 i smjesta u rez
	// ukoliko osobe imaju isto prezime tj. rez=0 provjerava se da li imaju ista imena
	if(rez==0) return ime.compareTo(o.ime);
	return rez;	
	}

/* predefinisanje metoda equals() nasljedjenog od univerzalne superklase Object
 * ovo je neophodno jer metodi contains(), indexOf() i lastIndexOf() porede trazeni i tekuci element Vektora
 * koristeci metod equals()
 * metod treba da vrati
 * 		true - ako je tekuci objekat atribut po atribut identican objektu o
 * 		false - ako nije
 */
public boolean equals(Object o){
	// ako o nije tipa osoba
	if (!(o instanceof Osoba)) return false;
	 // inace, metod treba da vrati true, ako metod compareTo() vrati 0
	// argument metoda compareTo() je tipa Osoba pa je kastovanje obavezno
	return compareTo((Osoba)o)==0;
}

@Override
public String toString() {
	// prvo prezime pa ime, da bi se vidjelo izvrseno sortiranje u leksikografskom poretku
	return prezime+" "+ime;
}




}
