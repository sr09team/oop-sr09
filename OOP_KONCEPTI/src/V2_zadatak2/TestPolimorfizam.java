package V2_zadatak2;

/* primjer ilustruje upotrebu apstraktne bazne klase i interfejsa
apstraktni metod je nasljedjen od interfejsa 
nije moguce kreirati objekat tipa apstraktne klase ali je moguce 
deklarisati promjenljivu tipa apstraktne klase i u njoj cuvati 
referencu na objekat proizvoljne izvedene
Ova promjenljiva se moze koristiti za polimorfno pozivanje metoda
dodato je i polimorfno pozivanje metoda obim() */

import java.util.Random;

public class TestPolimorfizam {
	public static void nain(String[] args) {

		// niz povrsi, da bi elementi bili tipa Krug,
		// Pravougaonik, Kvadrat, niz je tipa bazne klase
		Povrs povrsi[]={new Krug(new Tacka(),1),
				new Krug(new Tacka(1,1),2),
				new Pravougaonik(new Tacka(),3,4),
				new Kvadrat(new Tacka(4,6), 2)};

		// objekat klase Random iz paketa java.uti1
		// predstavlja generator pseudoslucajnih brojeva
		Random izbor = new Random();

		
		// 5 puta se slucajno bira element niza povrsi
		for(int i=0; i<5; i++){
			// p je promjenljiva tipa bazne klase koja cuva
			// referencu na objekat izvedene (jedan od uslova za polimorfizam)
			// izbor.nextInt(n) vraca slucajan cio broj iz opsega [0,...,n-1]
			// sto je validan indeks niza koji ima n elemenata

			// da bismo slucajno izabrali element niza povrsi,
			// nadjemo slucajan indeks za taj niz:
			// izbor.nextInt(povrsi.length)
			// a indeksiranjem niza tim indeksom dobijamo slucajan element niza:
			Povrs p = povrsi[izbor.nextInt(povrsi.length)];
			System.out.println(p); // polimorfno se poziva metod toString()
				// koji takodje ispunjava uslove za polimorfizam
			
			// polimorfno pozivanje metoda povrsina()
			System.out.println("Povrsina je " + p.povrsina());
			// polimorfno pozivanje metoda obim()
			System.out.println("0bim je " + p.obim());
			
			// pomocu promjenljive tipa bazne klase moguce je pozivati
			// samo polimorfne metode, ne i metode izvedenih klasa
			// koji ne ispunjavaju uslov za polimorfizam
			// za njihovo pozivanje neophodno je izvrsiti kastovanje nanize
			// u tip klase u kojoj je definisan metod koji se zeli pozvati
			// kastovanje nanize kroz hijerarhiju klasa:
			// provjera da 1i je dopusteno kastovati referencu iz p u tip Pravougaonik
			// (a s obzirom na to kako izgleda nasa hijerarhija klasa,
			// dopusteno je ako je u p referenca na objekat klase Pravougaonik ili Kvadrat
			// (instanceof vraca true ako je u p referenca na objekat koji je tipa Pravougaonik
			// ili njegove proizvoljne potklase)

			if(p instanceof Pravougaonik)
				// ako je kastovanje dopusteno (instanceof vratio true)
				// vrsimo kastovanje i pozivamo metod opisaniKrug() definisan u klasi Pravougaonik,
				// koji ne ispunjava uslove za polimorfno pozivanje

				System.out.println("0pisani "+((Pravougaonik)p).opisaniKrug());
			System.out.println();
		}
	}
}

