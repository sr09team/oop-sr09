package V2_zadatak2;

//klasa izvedena iz apstraktne klase Povrs kao bazne
//nije apstraktna jer predefinise metode povrsina()
//i obim() te je moguce praviti konkretne objekte klase Krug

public class Krug extends Povrs{
	//pored svega sto ima objekat bazne klase Povrs
	//(tj. centralne tacke) svaki krug ima i svoj poluprecnik
	private double radius;

	//konstruktor za kreiranje Kruga od svih neophodnih podataka
	public Krug(Tacka centar, double radius){
		//poziv prvog konstruktora u klasi Povrs
		super(centar);
		this.radius = radius;
		}

	//kopi-konstruktor
	public Krug(final Krug k){
		//poziv kopi-konstruktora bazne klase ciji je parametar tipa Povrs,
		//ali kako je Krug potklasa od Povrs kao stvarni argument se moze 
		//prosljediti i referenca na objekat klase Krug (vrsi se implicitno
		//kastovanje navise)
		super(k);
		//super(k.getCentar()); // alternativa za gornji red
		radius = k.radius;
		}
	
	//implementacija polimorfnog metoda koji ima isti potpis kao i metod u 
	//baznoj klasi, isti povratni tip kao metod u baznoj klasi i isti pristupni
	//atribut kao metod u baznoj klasi
	public double povrsina(){
		//PI je staticki clan klase Math iz paketa java.lang
		return radius*radius*Math.PI;
	}
	public double obim(){
		return 2*radius*Math.PI;
	}
	
	//String-reprezentacija kruga
	public String toString(){
		//pomocu super.toString() pozivamo nasljedjeni metod toString()
		//bazne klase, cije ime je skriveno istoimenim metodom ove klase
		//te mu se pristupa pomocu kljucne rijeci super
		return "krug " + super.toString() + " poluprecnik je "+ radius;
	}
}
