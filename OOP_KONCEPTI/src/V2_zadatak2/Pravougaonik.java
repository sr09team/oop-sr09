package V2_zadatak2;

public class Pravougaonik extends Povrs{
	// pored centralne tacke (bazni dio objekta)
	// svaki pravougaonik ima jos svoju sirinu i visinu
	private double a, b; // a-sirina, b-visina

	// kreiranje pravougaonika od svih neophodnih podataka
	public Pravougaonik(Tacka centar, double a, double b){
		// poziv prvog konstruktora iz bazne klase
		super(centar);
		this.a=a;
		this.b=b;
		}

	// kopi-konstruktor
	public Pravougaonik(final Pravougaonik p){
		// poziv kopi-konstruktora bazne klase (implicitna konverzija navise)
		super(p);
		a=p.a;
		b=p.b;
		}

	// racunanje dijagonale tekuceg pravougaonika kao duzine hipotenuze 
	// pravouglog trougla sa katetama a i b
	public double dijagonala(){
		return Math.sqrt(a*a+b*b);
		}

	// metod racuna i vraca krug opisan oko tekuceg pravougaonika
	public Krug opisaniKrug(){
		// krug ima istu centralnu tacku kao i pravougaonik pri cemu se 
		// centralnoj tacki, definisanoj kao private u baznoj klasi mora 
		//pristupiti pomocu nasljedjenog (public) metoda getCentar()

		// poluprecnik kruga opisanog oko pravougaonika jednak je polovini njegove dijagonale
		// obratiti paznju na to da se nasljedjeni metod getCentar() bazne klase i metod 
		// dijagona1a() definisan u ovoj klasi kada smo u tijelu ove klase pozivaju samo 
		// navodjenjem svog imena
		return new Krug(getCentar(), 0.5*dijagonala());
		}
	
	// metod koji ce biti nasljedjen u izvedenoj klasi Kvadrat
	public double getA(){
		return a;
	}

	// implementacija polimorfnog metoda (metod ima: isti potpis, isti povratni tip
	// i isti pristupni atribut kao i u baznoj klasi)racunanje povrsine tekuceg pravougaonika
	public double povrsina(){
		return a*b;
	}

	public double obim(){
		return 2*(a+b);
	}

	// Stringóreprezentacija pravougaonika
	public String toString(){
		return "pravougaonik " + super.toString() +" a= "+ a + " b= "+b;
	}
}
	


