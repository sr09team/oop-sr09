package V2_zadatak2;

//apstraktna bazna klasa

//klasa je apstraktna ako ima bar jedan apstraktni metod
//za apstraktnu klasu neophodno je u prvom redu definicije 
//te klase navesti kljucnu rijec abstract

//klasa je apstraktna jer nasljedjuje apstraktni metod povrsina() 
//od interfejsa Povrsina i ne predefinise ga, kao i apstraktni 
//metod obim() od interfejsa Obim, koji takodje ne predefinise

//moguce je da jedna klasa implementira dva ili vise interfejsa

public abstract class Povrs implements Povrsina, Obim{

	//svaka povrs ima svoju centralnu tacku
	private Tacka centar;

	//podrazumljevani konstruktor
	//kreira povrs sa centrom u koordinatnom pocetku
	public Povrs(){
		centar = new Tacka();
	}

	//kreiranje povrsi sa zadatom centralnom tackom
	public Povrs(Tacka centar){
		//2 varijante:
		this.centar = new Tacka(centar);
		//this.centar = centar; // 2. varijanta
	}

	//kopi-konstruktor kreira se povrs identicna postojecoj povrsi p
	public Povrs(final Povrs p){
		this(p.centar);
	}
	
	//metod koji ce biti nasljedjen u potklasama jer je public
	public Tacka getCentar(){
		return centar;
	}

	//metod racuna i vraca rastojanje od centra tekuce povrsi
	//do centra povrsi p, public je, pa ce biti nasljedjen u izvedenim klasama
	//parametar metoda je tipa bazne klase Povrs te ce kao stvarni argument 
	//moci da se prosljedi referenca na objekat tipa Povrs, ali i referenca 
	//na objekat proizvoljne izvedene klase (Krug, Pravougaonik, Kvadrat) pri
	//cemu se onda vrsi implicitno kastovanje navise kroz hijerarhiju klasa
	
	public double rastojanjeDoCentra(Povrs p){
		//poziv metoda rastojanje() iz klase Tacka
		return centar.rastojanje(p.centar);
	}
	//String-reprezentacija povrsi
	public String toString(){
		return "centar u tacki" + centar;
	}
}

