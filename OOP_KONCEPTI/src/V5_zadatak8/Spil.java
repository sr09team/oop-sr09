package V5_zadatak8;

import java.util.Collections;
import java.util.Stack;

public class Spil {

	// spil je predstavljen stekom karata jer se karte dijele
	// tako sto se jedna po jedna karta skida sa vrha spila
	private Stack<Karta> spil = new Stack<Karta>();

	public Spil() {
		
		// za svaku boju i svaku vrijednost kreira se odgovarajuca karta i smjesta na vrh spila
		// Boja.values() vraca niz svih konstanti enumeracije Boja
		for (Boja b : Boja.values())
			for (Vrijednosti2 v : Vrijednosti2.values())
				spil.push(new Karta(b, v));
	}

	// mijesanje elemenata kolekcije na slucajan nacin
	public void promjesaj() {
		Collections.shuffle(spil);
	}
	
	// kreira se prazna ruka, a zatim brKarata puta skida karta sa vrha spila i dodaje
	// u ruku. Metod vraca tako kreiranu ruku (ako na spilu nema dovoljno karata, bice 
	// dato onoliko koliko ih ima, a mozda i 0)
	// svaki put kad se nesto zeli skinuti sa vrha steka, treba prethodno provjeriti da 
	// stek nije prazan, jer u slucaju pokusaja skidanja elementa sa praznog steka
	// dolazi do izbacivanja izuzetka EmptyStackException
	public Ruka podjeliRuku(int brKarti) {
		Ruka ruka = new Ruka();
		for (int i = 0; i < brKarti; i++)
			if (!spil.empty())
				ruka.dodajKartu(spil.pop());
			else
				break;
		return ruka;
	}

	public String toString() {
		StringBuffer rez = new StringBuffer();
		for (Karta k : spil)
			rez.append(k + " ");
		return rez.toString();
	}
}
