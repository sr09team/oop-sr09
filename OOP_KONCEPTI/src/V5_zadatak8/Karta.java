package V5_zadatak8;

public class Karta implements Comparable<Karta>{
	
	//atributi su klase tipa odgovarajucih enumeracija
	private Boja boja;
	private Vrijednosti2 vrijednost;
	
	public Karta(Boja boja, Vrijednosti2 vrijednost) {
		this.boja = boja;
		this.vrijednost = vrijednost;
	}
	
	// klasa implementira interfejs Comparable<> pa ce biti
	// moguce sortiranje vektora karata koristenjem metoda Collections.sort()
	public int compareTo(Karta k){
		/* prvo se porede boje karte, konstante 
		 * enumeracije porede se metodom compareTo()
		 */
		int rez=boja.compareTo(k.boja);
		// ako su iste boje
		if(rez==0) 
			// jaca je ona karta koja ima jacu vrijednost
			return vrijednost.compareTo(k.vrijednost);
		
		// a inace ona koja ima jacu boju nezavisno od vrijednosti
		return rez;
	}
	@Override
	public String toString() {
		// string reprezentacija kostante enumeracije je samo ime koje smo joj dali
		return "(" + boja + " " + vrijednost+")";
	}
	
	

}
