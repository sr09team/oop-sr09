package V5_zadatak8;

import java.util.Collections;
import java.util.Vector;

public class Ruka {
	// karte u ruci predstavljene su vektorom karata koji je inicijalno prazan
	private Vector<Karta> ruka = new Vector<Karta>();

	// klasa nema konstruktor: kompajler ce generisati podrazumjevani
	// konstruktor sa praznim tijelom zahvaljujuci gornjoj naredbi, kreirana 
	// ruka ce imati prazan vektor karata
	//
	// ovaj konstruktor se pozivoa u metodu podijeliRuku() klase Spil
	
	// dodavanje zadate karte u ruku
	public void dodajKartu (Karta k){
		ruka.add(k);
	}

	// sortiranje karata u ruci po jacini, metod vraca sortiranu ruku karata
	// pa je moguce u test klasi u istoj naredbi podjeliti ruku karata, sortirati
	// karte u ruci i izvrsiti naredbu dodjele
	public Ruka sortiraj(){
		Collections.sort(ruka);
	return this;
	}

@Override
public String toString() {
	String rez="";
	for(Karta k : ruka){
		rez+=k+", ";
	}
	return rez;
}


}
