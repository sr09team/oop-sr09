package V5_zadatak8;

public class TestPoker {

	public static void main(String[] args) {
		// kreira se spil
		Spil spil = new Spil();
		System.out.println(spil);
		System.out.println();

		// spil se zatim mjesa
		spil.promjesaj();
		System.out.println(spil);
		
		// podjele se dvije ruke i sortiraju karte u njima
		Ruka R1 = spil.podjeliRuku(6).sortiraj();
		Ruka R2 = spil.podjeliRuku(6).sortiraj();
		
		// ispis karata u rukama
		System.out.println("R1: "+R1);
		System.out.println("R2: "+R2);
	}

}
