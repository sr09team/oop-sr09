package V3_zadatak2;

public class GenerickaKlasaKomparacija {

	public static <T extends Comparable<T>> T Maximum (T x, T y, T z){
		T max= x;
		if(y.compareTo(max)>0){
			max = y;
		}
		if(z.compareTo(max)>0){
		max = z;
		}
		return max;
	}
	
	public static void main(String[] args) {
		System.out.printf("Max od %d %d %d je %d :", 3, 4, 5, Maximum(3, 4, 5));
		System.out.printf("Max od %f %f %f je %f :", 3.3, 7.4, 9.5, Maximum(3.3, 7.4, 9.5));
		System.out.printf("Max od %s %s %s je %s :", "ananas","narandza", "breskva", Maximum("ananas" , "narandza", "breskva"));
		}

	}

