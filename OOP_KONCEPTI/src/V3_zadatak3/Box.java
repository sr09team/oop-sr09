package V3_zadatak3;

//genericka klasa Box
public class Box<T> {

private T t; // privatni clan koji moze biti promjenjivog tipa

public void dodaj(T t){
	this.t=t;
}

//get metod za prihvatanje vrijednosti privatnog clana t
public T get(){
	return t;
}

//glavni program - incijalizacija klasa generickog tipa i pozivi metoda genericke klase
public static void main(String[] args) {

Box<Integer> integerBox= new Box<Integer>(); // objekat integerBox �e imati integer vrijednost za t
Box<String> stringBox= new Box<String>(); // objekat stringBox �e imati string vrijednost za t

integerBox.dodaj(10);
stringBox.dodaj("Zdravo");

System.out.printf("Integer vrijednost: %d\n", integerBox.get());
System.out.printf("String vrijednost: %s\n", stringBox.get());

}
}
