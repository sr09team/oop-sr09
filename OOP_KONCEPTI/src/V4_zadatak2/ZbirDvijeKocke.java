package V4_zadatak2;

public class ZbirDvijeKocke {
	public static void main(String[] args) {
		System.out.println("Zbir dvije kocke Prosjecan broj bacanja");
		System.out.println("-------------------------------------—");
		for (int i = 2; i < 13; i++) System.out.printf("%9d %25.2f\n", i, prosjekZaZbir(i));
}

// metodu se salje zeljeni zbir koji se treba postici bacanjem dvije kocke         
public static int baciZaZbir(int zbir) {
	int brojBacanja = 0; // brojac bacanja dvije kocke
	KockaZaIgru kocka1 = new KockaZaIgru(); // prva kocka
	KockaZaIgru kocka2 = new KockaZaIgru(); // prva kocka
	do {
		kocka1.baci(); // baciti prvu kocku
		kocka2.baci(); // baciti drugu kocku
		brojBacanja++; // izracunati bacanje
	} while ((kocka1.broj + kocka2.broj)!=zbir);
	
	// metod vraca informacije o tome koliko je puta bilo potrebno
	// baciti kocke dok se ne dobije zeljeni zbir
	return brojBacanja;
}

public static double prosjekZaZbir(int zbir) {
	final int BROJ_PONAVLJANJA = 100000;
	int ukupnoBacanja = 0; // ukupan broj bacanja za dati zbir dvoje kocke
	// metod baciZaZbir(int zbir) se poziva 100 000 puuta nakon cega se
	// racuna koji je prosjecan broj bacanja potreban za odredjeni zbir
	for (int i = 0; i < BROJ_PONAVLJANJA; i++)
		ukupnoBacanja = ukupnoBacanja + baciZaZbir(zbir);
	return (double)ukupnoBacanja/BROJ_PONAVLJANJA;
}
}