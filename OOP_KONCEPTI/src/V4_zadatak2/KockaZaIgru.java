package V4_zadatak2;

public class KockaZaIgru {
	public int broj; //broj koji je pao

	public KockaZaIgru() { //konstruktor bez parametara
		baci();				// poziv metoda baci()
	}

	public KockaZaIgru(int n) {	//konstruktor s parametrom
		broj = n;
	}
	
	public void baci(){
		broj=(int)(Math.random()*6)+1;
	}
	
	
	
}
