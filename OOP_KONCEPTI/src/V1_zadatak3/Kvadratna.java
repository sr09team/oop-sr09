// sa standardnog ulaza se unosi dimenzija, a zatim i elementi
// kvadratne matrice realnih brojeva (tipa double)

// matrica se ispisuje u kvadratnom obliku
// pomocu standardnih for-petlji
// a zatim se ispisuje i zbir elemenata na glavnoj dijagonali

package V1_zadatak3;

import java.util.Scanner;

public class Kvadratna {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Unesite dimenziju kvadratne matrice");
		int n = sc.nextInt();

		// inicijalizujemo zbir elemenata na glavnoj dijagonali na 0
		double zbir = 0;

		// izdvajamo potrebnu kolicinu memorije za nasu matricu
		// prvo n je broj vrsta, a drugo broj kolona
		double a[][] = new double[n][n];

		System.out.println("Unesite elemente");
		// a.1ength je broj vrsta matrice
		for(int i=0; i<a.length; i++)
			// a[i].1ength je broj elemenata u i�toj vrsti (odnosno broj kolona)
			for(int j=0; j<a[i].length; j++){
				a[i][j] = sc.nextDouble();
				// ako je element na glavnoj dijagonali (i==j), dodamo ga na tekuci zbir
				if (i==j)
					zbir+=a[i][i];
		}
		sc.close();
		//zatvaramo resurs Scanner, u slucaju da izostavimo ovo Eclipse nam javlja upozorenje

		// ispis matrice u kvadratnom obliku, koristeci standardne for-petlje
		for(int i=0; i<a.length; i++){
			for(int j=0; j<a[i].length; j++)System.out.print(a[i][j]+" ");
			System.out.println();
		}

		System.out.println("Zbir elemenata na glavnoj dijagonali je: " + zbir);
	}
}

