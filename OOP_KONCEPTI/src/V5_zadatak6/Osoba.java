package V5_zadatak6;

import java.io.Serializable;
import java.util.Scanner;

/* klasa implementira interfejs Serializable kako bi
 * se postupkom serilizacije u fajl mogao upisati, i 
 * iz njega iscitati, vektor osoba
 * 
 * Serializable interfejs ne sadrzi ni jedan metod
 */

/* klasa implementira interfejs Comparable<Osoba> kako
 * bi bilo moguce sortiranje vektora osoba koristenjem
 * metoda Collections.sort() u interfejsu je deklarisan 
 * metod compareTo() pa se ovdje vrsi njegovo predefinisanje
 */
public class Osoba implements Comparable<Osoba>,Serializable{
private static final long serialVersionUID=1L;
private String ime;
private String prezime;

private static Scanner sc= new Scanner(System.in);


public Osoba(String ime, String prezime) {
	this.ime = ime;
	this.prezime = prezime;
}

public int compareTo(Osoba o){
	int rez= prezime.compareTo(o.prezime);
	if(rez==0) return ime.compareTo(o.ime);
	return rez;	
	}

/* predefinisanje metoda nasljedjenog od univerzalne superklase Object, 
 * kako bi ispravno funkcionisao metod contains() kojim se provjerava 
 * da li vektor Osoba sadrzi zadatu osobu
 */
public boolean equals(Object o){
return compareTo((Osoba)o)==0;
}

@Override
public String toString() {
	return prezime+" "+ime;
}

/* staticki metod za ucitavanje podataka o osobi sa standardnog ulaza i kreiranje objekata
 * koristenjem ucitanih podataka
 */
public static Osoba ucitajOsobu(){
	System.out.print("Unesite ime osobe: ");
	String ime=sc.next();
	System.out.print("Unesite prezime osobe: ");
	return new Osoba(ime,sc.next());
	}
}

