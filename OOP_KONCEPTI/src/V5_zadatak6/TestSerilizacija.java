package V5_zadatak6;

import java.util.Scanner;

public class TestSerilizacija {

	private static Scanner sc;

	public static void main(String[] args) {

		VektorOsoba osobe = new VektorOsoba();
		sc = new Scanner(System.in);
		Osoba neko = null;
		
		/* standardni nacin za rad sa opcijama:
		 * beskonacna petlja koja se naredbom return
		 * prekida kada korisnik izabere opciju za kraj
		 * (opcija 9)
		 */
		for (;;) {
			System.out.println("Unesite:");
			System.out.println(" 1 za unos nove osobe");
			System.out.println(" 2 za provjeru da li se neka osoba prijavila");
			System.out.println(" 3 za listanje u leksikogr. por.");
			System.out.println(" 9 za kraj (vrsi se automatsko upisivanje vektora u fajl)");

			int opcija = sc.nextInt();
			switch (opcija) {
			case 1:
				osobe.dodajOsobu(Osoba.ucitajOsobu());
				break;
			case 2:
				neko = Osoba.ucitajOsobu();
				if (osobe.sadrziOsobu(neko))
					System.out.println("Osoba " + neko + " se prijavila");
				else
					System.out.println("Osoba " + neko + " se NIJE prijavila");
				break;
			case 3:
				osobe.izlistajOsobe();
				break;
			case 9:
				osobe.sacuvaj();
				System.out.println("Kraj");
				return;
			default:
				System.out.println("Pogresna opcija, pokusajte ponovo");
				break;
			}
		}
	}

}
