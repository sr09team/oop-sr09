package V5_zadatak6;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.Vector;

public class VektorOsoba {
	
private Vector<Osoba> osobe = new Vector<Osoba>();
private File fajl = new File("C:\\Beg Java Stuff\\VektorOsoba.bin");

/* konstruktor radi na sledeci nacin:
 * ako se program pokrece prvi put fajl ne postoji pa ce vektor osobe biti prazan
 * ako je program vec pokretan, neposredno prije zavrsetka pozvan je metod sacuvaj 
 * koji je u fajl, koristeci serilizaciju, upisao tekuci sadrzaj vektora osobe. Fajl,
 *  dakle postoji, te se iz njega iscitava rezultat prethodnih izvrsavanja
 */
public VektorOsoba() {
	if(fajl.exists()){
		ObjectInputStream in;
		try {
			in = new ObjectInputStream(new FileInputStream(fajl));
			
			/* kastovanje je neophodno, kako u vrijeme kompajliranja ni na koji nacin nije moguce
			 * utvrditi da li se kastuje u stvarnu klasu objekta koji se cita iz fajla, upozorenje 
			 * kompajlera(zuti uzvicnik i vijugava linija) je neizbjezno
			 */
			osobe=(Vector<Osoba>)in.readObject();
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}
}

/* metod koji se zove neposredno prije zavrsetka izvrsavanja programa kako
 * bi u fajl upisao tekuci sadrzaj vektora (tom prilikom koristi se serilizacija)
 */
public void sacuvaj(){
	System.out.println("Upis vektora u file..");
	try {
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fajl));
		out.writeObject(osobe);
		out.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.exit(1);
	}
	System.out.println("Gotov upis");
}

/* metod koji se poziva kada korisnik izabere opciju 1 */
public void dodajOsobu(Osoba o){
	osobe.add(o);
}

/* metod koji se poziva kada korisnik izabere opciju 2 */
public boolean sadrziOsobu(Osoba o){
	return osobe.contains(o);
}

/* metod koji se poziva kada korisnik izabere opciju 3 */
public void izlistajOsobe(){
	Collections.sort(osobe);
	for(Osoba e:osobe){
		System.out.println(e);
	}
}
}
