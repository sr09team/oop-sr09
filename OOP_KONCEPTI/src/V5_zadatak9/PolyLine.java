package V5_zadatak9;

import java.util.LinkedList;

/* Klasa ilustruje upotrebu generickog tipa LinkedList<>
 * koja implementira uopstenu dvostruko povezanu listu
 * 
 * klasa ima dva konstruktora:
 * 		podrazumjevanji - kreira praznu listu
 * 		sa argumentom tipa Collections<> - kreira LinkedList<> objekat
 * 			koji inicijalno sadrzi reference na objekte kolekcije prosljedjene kao argument
 * 
 * metodi
 *		add(), addAll()
 *		addFirst(),	addLast()
 *		get(), getFirst(), getLast() remove(), removeFirst(), removeLast()
 *		set()
 *		size()
 *		iterator(),	listiterator(); 
 */

class PolyLine {

	// polikonalna linija je predstavljena povezanom listom tjemena
	LinkedList<Point> polyline = new LinkedList<Point>();

	// konstruktor kreira poligonalnu liniju od niza parova koordinata
	public PolyLine(double[][] coords) {
		// za svaki par koodrinata
		for (double[] xy : coords)
			// kreira se i tacka i dodaje na kraj liste
			polyline.add(new Point(xy[0], xy[1]));
	}
	
	// konstruktor kreira poligonalnu liniju od niza tacaka
	public PolyLine(Point[] points) {
		// svaka tacka..
		for (Point pt : points)
			// .. se dodaje na kraj povezane liste
			polyline.add(pt);
	}

	// dodavanje tacki zadate koordinatama
	public void addPoint(double x, double y) {
		// tacka se dodaje na kraj povezane liste tjemena
		polyline.add(new Point(x, y));
	}

	// dodavanje zadate tacke
	public void addPoint(Point pt) {
		// tacka se dodaje na kraj povezane liste tjemena
		polyline.add(pt);
	}

	@Override
	public String toString() {
		StringBuffer rez = new StringBuffer();
		for (Point pt : polyline)
			rez.append(" " + pt);
		return rez.toString();
	}

}
