package V5_zadatak9;

import java.net.StandardSocketOptions;

class TestLinkedList {

	public static void main(String[] args) {

		double[][] coords = { { 1, 2 }, { 2, 3 }, { 3, 4 }, { 4, 5 } };
		// poziv konstruktora koji ocekuje niz parova koordinata
		PolyLine poly1 = new PolyLine(coords);
		System.out.println("Poly1 = " + poly1);

		Point pt1 = new Point(10, 0);
		// dodavanje novog tjemena
		poly1.addPoint(pt1);

		System.out.println("Nakon dodavanja tjemena " + pt1 + ":");
		System.out.println("Poly1 = " + poly1);
		
		// kreiranje niza tacaka od niza parova koodrinata
		Point [] points= new Point[coords.length];
		for(int i=0; i<points.length;i++) 
			// koordinate i-te tacke nalaze se u i-toj vrsti
			// matrice coords[][]
			// x-koordinata je u prvoj, a y-koordinata u drugoj koloni te i-te vrste
			points[i]=new Point(coords[i][0],coords[i][1]);
		PolyLine poly2=new PolyLine(points);
		System.out.println("Poly2 = " + poly2);
		
	}

}
