package NitnaSigurnost_primjer;

public class Point {
	private int x,y;

	public Point() {
	}

	public void setPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
}
