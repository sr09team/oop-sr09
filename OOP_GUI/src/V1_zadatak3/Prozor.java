package V1_zadatak3;

//primjer je sasvim slican FlowLayout, osim sto je za rasporedjivanje 
//komponenata po centralnom panelu zaduzen GridLayout umjesto FlowLayout 
//manager-a
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Prozor extends JFrame {

	// clanica je definisana kao atribut, jer joj pristupamo
	// iz anonimne klase u kojoj obradjujemo dogadjaj: klik na dugme
	private JPanel centralniPanel;

	// brojac sluzi kako bi smo na dugmetu ispisivali i njegov redni broj
	private int broj = 0;

	public Prozor(String naslov) {

		// poziv konstruktora bazne klase, JFrame
		super(naslov);

		// dohvatimo kontent pane objekat koji predstavlja povrs naseg prozora.
		// Komponente se ne dodaju sirektno na prozor nego u njegov content pane
		Container content = getContentPane();

		// podrazumjevamo, layout manager za JFrame je BorderLayout i necemo ga
		// mijenjati u ovom primjeru (a mogli bi sa centent.setLayout(..);

		JButton dodajBt = new JButton("Dodaj");
		// dodavanje osluskivaca dogadjaja za dugme, kao objekta anonimne klase
		// koja implementira ActionListener interfejs

		dodajBt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// kreiramo dugme
				JButton dugmeBt = new JButton("Dugme" + ++broj);

				// postavljamo mu prioritetnu velicinu kako bi nam sva dugmad
				// bila iste velicine jer se podrazumjevana velicina dugmeta
				// prilagodjava tekstu koji na njemu pise (njegovoj labeli)
				dugmeBt.setPreferredSize(new Dimension(100, 30));

				// da bi dugme bilo velicine koju smo zadali, mozemo napraviti
				// pomocni panel (sa FlowLayout manager-om), na njega smjestiti
				// dugme a taj pomocni panel dodati na sentralni

				// JPanel pomPanel = new JPanel();
				// pomPanel.add(dugmeBt);
				// centralniPanel.add(pomPanel);
				// a zakomentarisati prvu narednu liniju koda kojom se dugmeBt
				// dodaje direktno na centralniPanel

				// dodajemo dugme na centralni panel, kojem možemo pristupiti iz
				// anonimne klase jer je definisan kao atribut spoljasnje klase
				// Prozor
				centralniPanel.add(dugmeBt);

				// VAZNO: pozivamo metod validate() kako bi dugme momentalno
				// bilo vidljivo
				centralniPanel.validate();
			}
		});

		// pomoćni panel na koji ćemo dodati dugme,
		// a taj panel (sa dugmetom) na povrs prozora;
		// ovo činimo kako nam dugme ne bi bilo razvučeno
		// cijelom širinom prozora (jer ćemo ga dodati na "sjever"
		// a "sjever" zauzima maksimalnu raspoloživu širinu)
		JPanel pomPanel = new JPanel();
		// podrazumijevani layout manager za JPanel je FlowLayout
		// (to nam odgovara, ne mijenjamo ga, on razmješta komponente po
		// redovima, koji su podrazumijevano centrirani. Mi ćemo u ovaj panel
		// dodati samo dodajBt, pa ce ono biti centrirano)
		pomPanel.add(dodajBt);

		// kada se komponenta dodaje u kontejner u kojem BorderLayout vrsi
		// razmjestaj komponenata, osim same komponente (prvi argument metoda
		// add), zadaje se i pozicija na koju se ona stavlja (konstanta tipa
		// String, koja moze biti: BorderLayout.NORTH,
		// BorderLayout.SOUTH,BorderLayout.East, BorderLayout.WEST,
		// BorderLayout.CENTER

		content.add(pomPanel, BorderLayout.NORTH);

		// u centralni dio stavljamo panel u kojem ce biti dodato novo dugme
		// svaki put kada se klikne na dugme "dodajBt" (kako zelimo da
		// pristupamo ovom panelu iz koda koji vrsi obradu dogadjaja klika na
		// dugme, a to ce biti unutar definicije anonimne klase koja
		// implementira ActionListener interfejs, ovaj panel se mora definisati
		// kao atribut klase Prozor

		centralniPanel = new JPanel(); // podrazumijevano FlowLayout

		// mijenjamo u GridLayout, sa parametrima 0, 1 (1 kolona, a vrsta
		// onoliko koliko komponenata budemo dodali)
		// --> komponente ce biti smještene jedna ispod druge a podijelit ce
		// raspoloživu visinu panela
		centralniPanel.setLayout(new GridLayout(0, 1));

		// probati i ovo umjesto gornja dva reda (matrica sa 3 vrste i 4 kolone)
		// (moguće je smjestiti i vise od 12 komponenata, pri čemu ce broj vrsta
		// biti 3, a broj kolona prigodno promijenjen)
		// centralniPanel = new JPanel(new GridLayout(3, 4));
		// moguće je zadati i horizontalni i vertikalni razmak izmedju susjednih
		// komponenata
		// centralniPanel = new JPanel(new GridLayout(3, 4, 15, 35));
		// a za objekat klase GridLayout mogu se pozivati i metodi setHgap() i
		// setVgap()

		// postavljanje ivice oko panela (plava linija)
		centralniPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE));

		content.add(centralniPanel, BorderLayout.CENTER);

	}
}
