package V1_zadatak2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

//izvodimo klasu naseg prozora iz JFrame i prilagodjavamo je 
//potrebama nase aplikacije
@SuppressWarnings("serial")
public class Prozor extends JFrame {

	// clanica je definisana kao atribut, jer joj pristupamo
	// iz anonimne klase u kojoj obradjujemo dogadjaj: klik na dugme
	private JPanel centralniPanel;

	// brojac sluzi kako bi smo na dugmetu ispisivali i njegov redni broj
	private int broj = 0;

	public Prozor(String naslov) {
		// poziv konstruktora bazne klase, JFrame
		super(naslov);

		// dohvatimo kontent pane objekat koji predstavlja povrs naseg prozora.
		// Komponente se ne dodaju sirektno na prozor nego u njegov content pane

		Container content = getContentPane();
		
		// podrazumjevamo, layout manager za JFrame je BorderLayout i necemo ga
		// mijenjati u ovom primjeru

		JButton dodajBt = new JButton("Dodaj");
		// dodavanje osluskivaca dogadjaja za dugme, kao objekta anonimne klase
		// koja implementira ActionListener interfejs
		dodajBt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				// kreiramo dugme
				JButton dugmeBt = new JButton("Dugme" + ++broj);

				// postavljamo mu prioritetnu velicinu kako bi nam sva dugmad
				// bila iste velicine jer se podrazumjevana velicina dugmeta
				// prilagodjava tekstu koji na njemu pise (njegovoj labeli)
				dugmeBt.setPreferredSize(new Dimension(100, 30));

				// dodajemo dugme na panel, kojem mozemo pristupiti iz anonimne
				// klase jer je definisan kao atribut spoljasne klase Prozor.
				centralniPanel.add(dugmeBt);

				// VAZNO: pozivamo metod validate() kako bi dugme momentalno
				// bilo vidljivo, ovaj metod je potrebno pozivati u situacijama
				// poput ove, kada se komponente dinamicki dodaju na povrs
				// prozora (unutar koda za obradu dogadjaja)
				centralniPanel.validate();
			}
		});

		// pomacni panel na koji cemo dodati dugme, a taj panel (sa dugmetom) na
		// povrs prozora. ovo cinimo kako nam dugme nebi bilo razvuceno cijelom
		// sirinom prozora (jer cemo ga dodati na "sjever" a "sjever" zauzima
		// maksimalnu raspolozivu sirinu)
		JPanel pomPanel = new JPanel();

		// podrazumjevani layout manager za JPanel je FlowLayout (to nam
		// odgovara, ne mjenjamo ga, on razmjesta po komponente redovim, koji su
		// podrazumjevano centrirani. Mi cemo u ovaj panel dodati samo dodajBT,
		// pa ce ono biti centrirano)
		pomPanel.add(dodajBt);

		// kada se komponenta dodaje u kontejner u kojem BorderLayout vrsi
		// razmjestaj komponenata, osim same komponente (prvi argument metoda
		// add), zadaje se i pozicija na koju se ona stavlja (konstanta tipa
		// String, koja moze biti: BorderLayout.NORTH,
		// BorderLayout.SOUTH,BorderLayout.East, BorderLayout.WEST,
		// BorderLayout.CENTER
		content.add(pomPanel, BorderLayout.NORTH);

		centralniPanel = new JPanel();
		// podrazumjevamo FlowLayout, redovi su centrirani, po 5 piksela razmaka
		// izmedju uzastopnih komponenata u jednom redu (po horizontali), kao i
		// izmedju komponenata u dva uzastopna reda ( po vertikali)

		// probati i ovo umjesto gornjeg reda:
		// centralniPanel=new JPanel(new FlowLayout(FlowLayout.LEFT));
		// redovi su lijepo poravnati, podrazumjevanih 5 piksela razmaka po
		// horizontali i po vertikali

		// probati i ovo (naredne tri linije):
		// centralniPanel = new JPanel();
		// FlowLayout flow = new FlowLayout (FlowLayout.RIGHT,40,25);
		// centalniPanel.setLAyout(flow);
		// redovi su desno poravnati, razmaci su 40 piksela po horizontali i 25
		// po vertikali

		// horizontalni razmak na nivou cijelog kontejnera se moze promjeniti sa
		// flow.setHgap(75);
		// a vertikalni sa
		// flow.setVgap(50);

		// postavljpanje ivice oko panela (plava linija)
		centralniPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		content.add(centralniPanel, BorderLayout.CENTER);

	}
}
