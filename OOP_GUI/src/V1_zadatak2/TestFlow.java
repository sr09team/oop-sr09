package V1_zadatak2;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class TestFlow {

	public static void main(String[] args) {
		// kako bi se izbjeka mogucnost dead-lock-a kod za kreiranje dogadjaja
		// se izvrsava u niti za obradu dogadjaja, sto se postize pozivom metoda
		// SwingUtilities.invokeLater(); metod kao argument ocekuje referencu na
		// objekat klase koja implementira Runnable interfejs. Kreiramo ga kao
		// objekat anonimne klase. U metodu run() pozivamo nas staticki metod
		// createGUI() gdje kreiramo prozor aplikacije i prikazujemo ga
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				createGUI();

			}
		});

	}

	protected static void createGUI() {
		// kreiramo prozor
		Prozor prozor = new Prozor("FlowLAyout");
		// postavljamo mu poziciju i velicinu
		prozor.setBounds(50, 100, 600, 400);
		// kad se klikne na X ili na Close, aplikacija ce se zavrsiti
		prozor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// prikazujemo prozor
		prozor.setVisible(true);

	}

}
