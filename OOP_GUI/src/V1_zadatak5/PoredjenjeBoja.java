package V1_zadatak5;

import java.awt.Color;
import javax.swing.JButton;

// ispravno je porediti dvije boje na jednakost pomocu metoda 
// equals(), a ne pomocu operatora ==
public class PoredjenjeBoja {

	public static void main(String[] args) {
		Color mojaCrvena = new Color(255, 0, 0);
		Color javaCrvena = Color.RED;
		System.out.println("MojaCrvena: " + mojaCrvena);
		System.out.println("JavaCrvena: " + javaCrvena);
		System.out.println();
		System.out.println("Da li su boje iste?");

		if (mojaCrvena == javaCrvena)
			System.out.println("Boje su iste");
		else
			System.out.println("Boje nisu iste");

		if (mojaCrvena.equals(javaCrvena))
			// ako su nam sve boje koje smo koristili bile konstante oblika
			// Color.xxx, onda ce program raditi na ocekivani nacin i ako
			// poredimo sa ==, ali ne bi se trebalo oslanjati na to
			System.out.println("Jednake su.");

		JButton zutoDugme = new JButton("Zuto dugme..");
		zutoDugme.setBackground(Color.YELLOW);
		if (zutoDugme.getBackground() == Color.YELLOW)
			System.out.println("Dugme jeste zuto");
	}

}
