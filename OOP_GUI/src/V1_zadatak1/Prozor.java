package V1_zadatak1;

/* komponente u centralnom panelu rasporedjuje BorderLayout
 * bice rasporedjenje slucajnim redom, sto postizemo tako sto 
 * konstante (tipa String) stavimo na stek, stek slucajno promjesamo,
 * a onda ih jednu po jednu skidamo sa steka i stavljamo komponentu (dugme)
 * na mjesto odredjeno konstantom.  Na samom dugmetu ce biti ispisana
 * String-reprezentacija konstante (South, North, West, East ili Center)
 */

/* u "normalnim" primjenama komponente se smijestaju tacno na odredjene 
 * pozicije (a ne slucajno), ali ovdje je poenta vidjeti kako se "sire"
 * pojedine komponente kada nije popunjeno svih 5 mjesta, kao i da to ne 
 * zavisi od redoslijeda kojim se komponente smijestaju u kontejner kojim 
 * "caruje" BorderLayout
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Stack;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

// izvodimo klasu naseg prozora iz JFrame i prilagodjavamo je 
// potrebama nase aplikacije
@SuppressWarnings("serial")
public class Prozor extends JFrame {
	// clanica je definisana kao atribut, jer joj pristupamo
	// iz anonimne klase u kojoj obradjujemo dogadjaj: klik na dugme
	private JPanel centralniPanel;
	private Stack<String> strane = new Stack<String>();

	public Prozor(String naslov) {
		// poziv konstruktora bazne klase, JFrame
		super(naslov);
		// stavljamo konstante na stek

		// centralna komponenta se siri tako da maksimalno popuni raspoloziv
		// prostor
		// ako nema komponenata da drugim mjestima zauzet ce citav kontejnere
		strane.push(BorderLayout.CENTER);
		// popunjava cijelu sirinu kontejnera, fiksne je visine
		strane.push(BorderLayout.NORTH);
		// popunjava cijelu sirinu kontejnera, fiksne je visine
		strane.push(BorderLayout.SOUTH);
		// fiksne sirine, ako nema sjevera ili juga, proteze se od sjeverne,
		// odnosno juzne ivice kontejnera
		strane.push(BorderLayout.EAST);
		// fiksne sirine, ako nema sjevera ili juga, proteze se od sjeverne,
		// odnosno juzne ivice kontejnera
		strane.push(BorderLayout.WEST);

		// slucajno ih promjesamo
		Collections.shuffle(strane);

		// dohvatimo kontent pane objekat koji predstavlja povrs naseg prozora.
		// Komponente se ne dodaju sirektno na prozor nego u njegov content pane
		Container content = getContentPane();
		// po-drazumjevamo, layout manager za JFrame je BorderLayout i necemo ga
		// mijenjati u ovom primjeru ( a mogli smo sa content.setLayout(..)

		// ako je lokalna promjenjiva metoda deklarisana kao final, moze joj se
		// pristupiti iz koda anonimne klase cija definicija se nalazi ispod
		// deklaracije final lokalne promjenjive (zelecemo to: onemogucujemo
		// dugme iz koda za obradu dogadjaja nad njim onda kada stek postane
		// prazan
		final JButton dodajBt = new JButton("Dodaj");
		// dodavanje osluskivaca dogadjaja za dugme, kao objekta anonimne klase
		// koja implementira ActionListener interfejs
		dodajBt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// skidamo konstantu sa steka (prvi put kad se klikne na dugme
				// na steku ima 5 elemenata (nije prazan), ako se nakon ovoga
				// stek isprazni onemogucicemo dugme, tako da nece dolaziti do
				// EmptyStackException)
				String strana = strane.pop();
				// kreiramo dugme na kojem pise pozicija gdje ce biti smjesteno
				JButton dugmeBt = new JButton(strana);
				// dodajemo dugme na panel, kojem mozemo pristupiti iz anonimne
				// klase jer je definisan kao atribut spoljasne klase Prozor.
				// Dugme dodajemo na poziciju odredjenu konstantom koju smo
				// skinuli sa steka
				centralniPanel.add(dugmeBt, strana);
				// VAZNO: pozivamo metod validate() kako bi dugme momentalno
				// bilo vidljivo
				centralniPanel.validate();
				// VAZNO: pozivamo metod validate() kako bi dugme momentalno
				// bilo vidljivo
				centralniPanel.validate();

				// ako je stek postao prazan, onemogucimo dugme za dodavanje
				// komponenata u centralni panel (proglasili smo dugme za final
				// lokaciju promjenjivu, pa mu ovdje mozemo pristupiti.
				// alternativa je bila da ono bude atribut spoljasne klase)
				if (strane.empty())
					dodajBt.setEnabled(false);
			}
		});

		// pomacni panel na koji cemo dodati dugme, a taj panel (sa dugmetom) na
		// povrs prozora. ovo cinimo kako nam dugme nebi bilo razvuceno cijelom
		// sirinom prozora (jer cemo ga dodati na "sjever" a "sjever" zauzima
		// maksimalnu raspolozivu sirinu)
		JPanel pomPanel = new JPanel();
		// podrazumjevani layout manager za JPanel je FlowLayout (to nam
		// odgovara, ne mjenjamo ga, on razmjesta po komponente redovim, koji su
		// podrazumjevano centrirani. Mi cemo u ovaj panel dodati samo dodajBT,
		// pa ce ono biti centrirano)
		pomPanel.add(dodajBt);
		// kada se komponenta dodaje u kontejner u kojem BorderLayout vrsi
		// razmjestaj komponenata, osim same komponente (prvi argument metoda
		// add), zadaje se i pozicija na koju se ona stavlja (konstanta tipa
		// String, koja moze biti: BorderLayout.NORTH,
		// BorderLayout.SOUTH,BorderLayout.East, BorderLayout.WEST,
		// BorderLayout.CENTER
		content.add(pomPanel, BorderLayout.NORTH);

		// u centralni dio stavljamo panel u kojem ce biti dodato novo dugme
		// svaki put kada se klikne na dugme "dodajBt" (kako zelimo da
		// pristupamo ovom panelu iz koda koji vrsi obradu dogadjaja klika na
		// dugme, a to ce biti unutar definicije anonimne klase koja
		// implementira ActionListener interfejs, ovaj panel se mora definisati
		// kao atribut klase Prozor
		centralniPanel = new JPanel();
		// podrazumjevamo FlowLayout, redovi su centrirani, po 5 piksela razmaka
		// izmedju uzastopnih komponenata u jednom redu (po horizontali), kao i
		// izmedju komponenata u dva uzastopna reda ( po vertikali)

		// mi umjesto tog podrazumjevanog FlowLayout postavljamo BorderLayout
		centralniPanel.setLayout(new BorderLayout());

		// moze i sa zadavanjem horizontalnog i vertikalnog razmaka izmedju
		// susjednih komponenata
		// centeralniPanel.setLayout(new BorderLayout(15,35));

		// postavljanje ivice oko panela (plava linija)
		centralniPanel.setBorder(BorderFactory.createLineBorder(Color.blue));
		content.add(centralniPanel, BorderLayout.CENTER);
	}

}
