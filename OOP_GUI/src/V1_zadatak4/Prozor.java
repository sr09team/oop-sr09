package V1_zadatak4;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class Prozor extends JFrame {
	// komponente kojima se pristupa iz koda za obradu dogadjaja definisane su
	// kao atributi klase

	// sirina polja bice 20*<sirina_slova_"m">
	private JTextField imePrezimeTF = new JTextField(20);
	private JTextField godisiteTF = new JTextField(5);

	private JRadioButton leksikografskiRB = new JRadioButton("po prezimenu i imenu", true);
	private JRadioButton poGodinamaRB = new JRadioButton("po godinama starosti, rastuce");

	private JTextField imePrezime2TF = new JTextField(20);
	private JTextField godisite2TF = new JTextField(5);

	// visina: 4 reda, a sirina 20*<sirina_slova_"m">
	private JTextArea oblast = new JTextArea(4, 20);
	private Vector<Osoba> osobe = new Vector<Osoba>();

	public Prozor(String naslov) {
		super(naslov);
		Container content = getContentPane();
		// smjestat cemo panele jedan ispod drugoga
		content.setLayout(new GridLayout(0, 1));

		JPanel imePrezimePanel = new JPanel();
		imePrezimePanel.add(new JLabel("Ime i prezime: "));
		imePrezimePanel.add(imePrezimeTF);
		imePrezimePanel.add(new JLabel("godiste"));
		imePrezimePanel.add(godisiteTF);

		content.add(imePrezimePanel);

		JPanel unesiPonistiPanel = new JPanel();
		JButton unesi = new JButton("Unesi");
		JButton ponisti = new JButton("Ponisti");
		unesiPonistiPanel.add(unesi);
		unesiPonistiPanel.add(ponisti);
		unesi.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String imePrezimeStr = imePrezimeTF.getText().trim();
				String godisteStr = godisiteTF.getText().trim();
				int indexBeline = imePrezimeStr.indexOf(' ');
				if (indexBeline == -1) {
					imePrezimeTF.setText("Neispravan unos");
					return;
				}
				int godiste = 0;

				try {
					godiste = Integer.parseInt(godisteStr);
				} catch (NumberFormatException e1) {
					imePrezimeTF.setText("Neispravno godiste");
				}

				String ime = imePrezimeStr.substring(0, indexBeline);
				String prezime = imePrezimeStr.substring(indexBeline + 1).trim();

				osobe.add(new Osoba(ime, prezime, godiste));
			}
		});
		ponisti.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				imePrezimeTF.setText("");
				godisiteTF.setText("");
			}
		});
		content.add(unesiPonistiPanel);

		// radio-dugmad se smijestaju jedan ispod drugoga
		JPanel radioDugmadPanel = new JPanel();
		radioDugmadPanel.add(leksikografskiRB);
		radioDugmadPanel.add(poGodinamaRB);

		// osim na povrs prozora, radio dugmad koja pripadaju istoj grupi moraju
		// se dodati i u objekat tipa ButtonGroup, koji vodi racuna da je uvjek
		// tacno jedno radio-dugme iz grupe selektovano
		ButtonGroup bg = new ButtonGroup();
		bg.add(leksikografskiRB);
		bg.add(poGodinamaRB);
		radioDugmadPanel
				.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE), "sortiranje"));
		content.add(radioDugmadPanel);

		JPanel sortirajPanel = new JPanel();
		JButton sortirajDugme = new JButton("Sortiraj i ispisi rezultat");
		sortirajPanel.add(sortirajDugme);
		sortirajDugme.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (leksikografskiRB.isSelected())
					Osoba.setSortLeksikografski(true);
				else
					Osoba.setSortLeksikografski(false);
				Collections.sort(osobe);
				if (osobe.size() >= 1) {
					imePrezime2TF.setText(osobe.get(0).getIme() + " " + osobe.get(0).getPrezime());
					godisite2TF.setText(osobe.get(0).getGodiste() + "");
					String rez = "";
					for (Osoba o : osobe)
						rez += o + "\n";
					oblast.setText(rez);
				}
			}
		});
		content.add(sortirajPanel);

		JPanel imePrezime2Panel = new JPanel();
		imePrezime2Panel.add(new JLabel("ime i prezime: "));
		imePrezime2Panel.add(imePrezime2TF);
		imePrezime2Panel.add(new JLabel("Godiste: "));
		imePrezime2Panel.add(godisite2TF);

		// onemogucavanje unosa korisnika u tekstualno polje imePrezime2TF
		imePrezime2TF.setEditable(false);
		godisite2TF.setEditable(false);
		content.add(imePrezime2Panel);

		// onemogucuje se unos korisnika u oblast
		oblast.setEditable(false);

		// da bi se po potrebi pojavili skrol barovi oko oblasi, ona se mora
		// smjestiti u JScrollPane
		JScrollPane pane = new JScrollPane(oblast);

		content.add(pane);
		// ako je komentarise gornja a uklone komentari sa 3 preostale linije
		// JPanel oblastPanel - new JPanel();
		// oblastPanel.add(pane);
		// content.add(oblastPanel);
		// ... vazit ce dimenzije 4 reda x 20 kolona koje smo zadali za oblast
		// prilikom njenog lreiranja

	}
}
