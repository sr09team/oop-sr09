package V1_zadatak4;

public class Osoba implements Comparable<Osoba> {
	private String ime, prezime;
	private int godiste;

	// vrijednost ce biti mijenjanja u klasi Prozor u zavisnosti od toga da li
	// je prilikom klika na dugme za sortiranje i ispis selektovano ili nije
	// radio-dugme za leksikografsko sortiranje
	private static boolean sortLeksikografski = true;

	public Osoba(String ime, String prezime, int godiste) {
		this.ime = ime;
		this.prezime = prezime;
		this.godiste = godiste;
	}

	public static void setSortLeksikografski(boolean sortLeksikografski) {
		Osoba.sortLeksikografski = sortLeksikografski;
	}

	@Override
	public int compareTo(Osoba o) {
		// u zavisnosti od izbora korisnika vrsit ce se sortiranje po jednom od
		// dva moguca kriterijuma
		if (sortLeksikografski) {
			int rez = prezime.compareTo(o.prezime);
			if (rez == 0)
				return ime.compareTo(o.ime);
			return rez;
		} else {
			return o.godiste - godiste;
		}

	}

	public String getIme() {
		return ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public int getGodiste() {
		return godiste;
	}

	@Override
	public String toString() {
		return ime + " " + prezime + " " + godiste;
	}

}
