package V1_zadatak4;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class TestOsobe {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				createGUI();

			}
		});
	}

	public static void createGUI() {

		Prozor prozor = new Prozor("Osobe");

		// dimenzija prozora bice prilagodjena njegovom sadrzaju
		prozor.pack();

		// centriramo prozor na ekranu
		Dimension velicinaProzora = prozor.getSize();
		Toolkit theKit = Toolkit.getDefaultToolkit();
		Dimension velicinaEkrana = theKit.getScreenSize();
		prozor.setLocation((velicinaEkrana.width - velicinaProzora.width) / 2,
				(velicinaEkrana.height - velicinaProzora.height) / 2);

		prozor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		prozor.setVisible(true);
	}

}
