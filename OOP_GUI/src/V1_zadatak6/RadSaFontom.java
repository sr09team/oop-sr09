package V1_zadatak6;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class RadSaFontom {

	private static JFrame prozor = new JFrame("Fontovi");

	public static void main(String[] args) {
		Container content = prozor.getContentPane();
		content.setLayout(new FlowLayout());

		Font mojFont = new Font("Serif", Font.ITALIC + Font.BOLD, 12);
		JLabel labela1 = new JLabel(mojFont.getFamily() + ", " + mojFont.getStyle() + ", " + mojFont.getSize());
		// Serif, ITALIC+BOLD, 12
		labela1.setFont(mojFont);
		content.add(labela1);

		// isti takav font, samo sa promjenjenim stilom
		Font mojFont2 = mojFont.deriveFont(Font.ITALIC);
		JLabel labela2 = new JLabel(mojFont2.getFamily() + ", " + mojFont2.getStyle() + ", " + mojFont2.getSize());
		labela2.setFont(mojFont2);
		content.add(labela2);

		// velicina uvecana za 5 piksela, argument je tipa float!!! zbog cega i
		// stoji f iza 5
		Font mojFont3 = mojFont.deriveFont(mojFont.getSize() + 5f);
		JLabel labela3 = new JLabel(mojFont3.getFamily() + ", " + mojFont3.getStyle() + ", " + mojFont3.getSize());
		labela2.setFont(mojFont3);
		content.add(labela3);

		prozor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		prozor.pack();
		prozor.setVisible(true);

	}

}
