package V1_zadatak7;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MenuWinBuilder {

	private JFrame frame;
	private JTextField txtNarudzba;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuWinBuilder window = new MenuWinBuilder();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MenuWinBuilder() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		
		JPanel panelMenu = new JPanel();
		frame.getContentPane().add(panelMenu, "name_611548729638067");
		
		JPanel panelStandard = new JPanel();
		frame.getContentPane().add(panelStandard, "name_611551488495993");
		panelStandard.setLayout(null);

		JButton btnStMenu = new JButton("Standardni menu");
		btnStMenu.setBounds(52, 84, 113, 23);
		btnStMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelMenu.setVisible(false);
				panelStandard.setVisible(true);
			}
		});
		panelMenu.setLayout(null);
		panelMenu.add(btnStMenu);
		
		JPanel panelVeg = new JPanel();
		frame.getContentPane().add(panelVeg, "name_611553283621219");
		panelVeg.setLayout(null);

		JButton btnVegMenu = new JButton("Vegetarijanski menu");
		btnVegMenu.setBounds(251, 84, 129, 23);
		btnVegMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelMenu.setVisible(false);
				panelVeg.setVisible(true);
			}
		});
		panelMenu.add(btnVegMenu);
		
		
		JLabel lblNewLabel = new JLabel("Vasa narudzba je:");
		lblNewLabel.setBounds(80, 137, 87, 14);
		panelMenu.add(lblNewLabel);
		
		txtNarudzba = new JTextField();
		txtNarudzba.setBounds(105, 162, 250, 43);
		panelMenu.add(txtNarudzba);
		txtNarudzba.setColumns(10);
		
		
		JCheckBox chckbxPiletina = new JCheckBox("Piletina");
		chckbxPiletina.setBounds(102, 74, 97, 23);
		panelStandard.add(chckbxPiletina);
		
		JCheckBox chckbxPomfrit = new JCheckBox("Pomfrit");
		chckbxPomfrit.setBounds(102, 100, 97, 23);
		panelStandard.add(chckbxPomfrit);
		
		JCheckBox chckbxPice = new JCheckBox("Pice");
		chckbxPice.setBounds(102, 126, 97, 23);
		panelStandard.add(chckbxPice);
		
		JButton btnGotovoS = new JButton("Gotovo");
		btnGotovoS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelStandard.setVisible(false);
				panelMenu.setVisible(true);
			String s ="";
			if(chckbxPiletina.isSelected()) s+="Piletina ";
			
			if(chckbxPomfrit.isSelected()) {
				if(s.length()>1) s+=", ";
				s+="Pomfrit ";
			}
			if(chckbxPice.isSelected()) {
				if(s.length()>1) s+=", ";
				s+="Pice";
			}
			txtNarudzba.setText(s);
			}
		});
		btnGotovoS.setBounds(102, 178, 89, 23);
		panelStandard.add(btnGotovoS);
		
		
		JCheckBox chckbxVoce = new JCheckBox("Voce");
		chckbxVoce.setBounds(88, 60, 97, 23);
		panelVeg.add(chckbxVoce);
		
		JCheckBox chckbxPovrce = new JCheckBox("Povrce");
		chckbxPovrce.setBounds(89, 96, 97, 23);
		panelVeg.add(chckbxPovrce);
		
		JCheckBox chckbxVoda = new JCheckBox("Voda");
		chckbxVoda.setBounds(91, 135, 97, 23);
		panelVeg.add(chckbxVoda);
		
		JButton btnGotovotV = new JButton("Gotovo");
		btnGotovotV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s ="";
				if(chckbxVoce.isSelected()) s+="Voce ";
				
				if(chckbxPovrce.isSelected()) {
					if(s.length()>1) s+=", ";
					s+="Povrce ";
				}
				if(chckbxVoda.isSelected()) {
					if(s.length()>1) s+=", ";
					s+="Voda";
				}
				txtNarudzba.setText(s);
				
				panelMenu.setVisible(true);
				panelVeg.setVisible(false);
			}
		});
		btnGotovotV.setBounds(94, 176, 89, 23);
		panelVeg.add(btnGotovotV);
	}
}
