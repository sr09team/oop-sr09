/* program ilustruje citanje iz String—a,pomješanu upotrebu metoda next(),
nextInt(), nextDoub|e()(koji preskacu beline - blankove, tabulatore, nove
redove)i metoda nextLine() koji vraca ostataktekuće linije (ne ukljucujući
oznaku za kraj reda)i pozicionira se na pocetak sljedeée linije i upotrebu metoda
useDelimiter()
NE POSTOJI metod nextChar()
čitanje jednog karaktera: char c = sc.next().charAt(0);
*/

package uvod_vjezbe1;

import java.io.*;
import java.util.Scanner;

public class Zadatak1 {

	public static void main(String[] args) {

		try {
			Scanner sc = new Scanner(new File("podaci.txt"));
			String dan = sc.next(); // "ponedjeljak"

			String datum = sc.next(); // "05.03.2012."
			// "razbijamo" datum na dan, mjesec i godinu, tipa int,
			// tako što ćemo citati iz stringa "datum", a delimiter
			// će biti " "
			Scanner scDatum = new Scanner(datum);
			scDatum.useDelimiter("\\.");
			int d = scDatum.nextInt(); // 10
			int m = scDatum.nextInt(); // 3
			int g = scDatum.nextInt(); // 2014
			scDatum.close();

			String vrijeme = sc.next(); // "16:15"
			// "razbijamo" vrijeme na sate i minute, tipa int,
			// tako što ćemo čitati iz stringa "vrijeme", a delimiter
			// ce biti " "
			Scanner scVrijeme = new Scanner(vrijeme);
			scVrijeme.useDelimiter(":");
			int sat = scVrijeme.nextInt(); // 16
			int minut = scVrijeme.nextInt(); // 15
			scVrijeme.close();

			int brCasova = sc.nextInt(); // 2
			sc.nextLine(); // !!! ostatak linije u kojoj je 2

			String nazivPredmeta = sc.nextLine();
			String sala = sc.next();

			System.out.println("Procitano:");
			System.out.println("Dan: " + dan + "\nDatum: " + d + "." + m + "." + g + ".");
			System.out.println("Vrijeme: " + sat + " sati i " + minut + " minuta.");
			System.out.println(brCasova + "casa vjezbi.");
			System.out.println("Predmet: " + nazivPredmeta);
			System.out.println("Sala: " + sala);
			sc.close();

		} catch (FileNotFoundException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
