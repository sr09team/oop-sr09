/* program ilustruje citanje podataka iz fajla brojevi.txt koji se nalazi u
direktorijumu projekta, i upotrebu metoda hasNext() */

package uvod_vjezbe1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadatak2 {

	public static void main(String[] args) {
		try {
			Scanner sc = new Scanner(new File("brojevi.txt"));

			while (sc.hasNextInt()) {
				int broj = sc.nextInt();
				System.out.println(broj);
			}
			sc.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
