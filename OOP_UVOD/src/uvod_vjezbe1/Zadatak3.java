/* program traži od korisnika dimenziju cijelobrojnog niza a zatim i same
elemente, na izlazu se štampa zbir i aritmeticka sredina elemenata niza */

package uvod_vjezbe1;

import java.util.Scanner;

public class Zadatak3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Unesite dimenziju niza");
		int n = sc.nextInt();

		int a[] = new int[n];

		System.out.println("Unesite elemente: ");
		// U promjenljvoj ZBIR akumuliramo zbir unijetih elemenata
		// lokalne promenljive moramo sami inicijalizovati
		// njihova inicijalizacija ne vrši se automatski

		int zbir = 0;
		for (int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
			zbir += a[i];
		}

		System.out.println("Zbir unjetih elemenata je " + zbir);
		double arSred = (double) zbir / n;
		System.out.println("Ar. sred. je " + arSred);
		sc.close();
	}
}
