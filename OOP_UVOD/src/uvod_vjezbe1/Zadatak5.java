package uvod_vjezbe1;

public class Zadatak5 {

	public static void main(String[] args) {
		System.out.println();
		System.out.println("Operator \"+\" je asocijativan s lijeva nadesno: ");
		
		// String + broj + broj
		System.out.println("Pedeset pet je "+ 5 + 5);
		
		// broj + broj + String
		System.out.println(5+5+" je deset");

		//dobijanje String-prezentacije podatka primitivnog tipa
		System.out.println("String-reprezentacija podatka primitivnog tipa");
		int iBroj= 123;
		double dBroj= 123.456;
		char c = 'a';
		boolean logicki = false;
		
		// 1. nacin - staticki metod toString() odgovarajuće Wrapper-klase
		System.out.println("1. nacin, staticki metod toString() odgovarajuće Wrapper klase");
		String pom =Integer.toString(iBroj);
		System.out.println(pom);
		pom = Double.toString(dBroj);
		System.out.println(pom);
		pom = Character.toString(c);
		System.out.println(pom);
		pom = Boolean.toString(logicki);
		System.out.println(pom);
		
		System.out.println("2. nacin, staticki metod value0f() klase String");
		pom = String.valueOf(iBroj);
		System.out.println(pom);
		pom = String.valueOf(dBroj);
		System.out.println(pom);
		pom = String.valueOf(c);
		System.out.println(pom);
		pom = Boolean.toString(logicki);
		System.out.println(pom);

		System.out.println("3. nacin, sabiranje sa praznim Stringom");
		pom = iBroj +"";
		System.out.println(pom);
		pom = dBroj+"";
		System.out.println(pom);
		pom = c+"";
		System.out.println(pom);
		pom = logicki+"";
		System.out.println(pom);
		
		System.out.println("*************************************************");
		System.out.println();

		/* metod 1ength() s1uži za racunanje duižine Stringa */
		String s="objektno";
		System.out.println("Duzina stringa \"" + s + "\" je " + s.length());

		// metod se može zvati i direktno za String konstantu:
		System.out.println("Duzina stringa \"programiranje\" je " +	 "programiranje".length());

		System.out.println("*************************************************");
		System.out.println();

		//		metod charAt() vraća karakter koji se u stringu
		//		nalazi na zadatoj poziciji, pozicija se zadaje
		//		kao argument metoda. Pozicije se broje od 0.
		//		Pokušaj dohvatanja karaktera sa negativne i1i
		//		policije koja je veća ili jednaka od duižine
		//		String-a dovodi do izbacivanja izuzetka tipa
		//		StringIndexOutOfBoundsException
		System.out.println("Prvi karakter stringa \"" + s + "\": " + s.charAt(0));

		System.out.println("*************************************************");
		System.out.println();

		// == "ne cita šta piše u Stringovima
		// već samo "gleda" da 1i se radi o istom objektu u memoriji
		String s1= "Dva studenta";
		String s2= "Dva";
		String s3= "studenta";
		s3=s1+s2;
		
		System.out.println("s1=\""+s1+"\"");
		System.out.println("s3=\""+s3+"\"");
		
		if(s1==s3) 
			System.out.println("== vratio true.");
		else
			System.out.println("== vratio false.");
		
		if(s1.equals(s3)) 
			System.out.println("equals vratio true.");
		else
			System.out.println("equals vratio false.");

		System.out.println( );
		
		s3="DVA studenta";
		System.out.println("s1=\""+s1+"\"");
		System.out.println("s3=\""+s3+"\"");

		if(s1.equals(s3)) 
			System.out.println("equals vratio true.");
		else
			System.out.println("equals vratio false.");

		if(s1.equalsIgnoreCase(s3))
			System.out.println("equalsIgnoreCase vratio true.");
		else
		System.out.println("equalsIgnoreCase vratio false.");
		
		System.out.println();

		// promjenljive s1 i s4 sadrže reference na isti objekat
		// u memoriji jer kompajler, ako u tekstu programa
		// pronadje dva i1i više istih String konstanti
		// ne kreira poseban objekat za svaku od njih,
		// nego samo jedan, i zbog toga == za njih vrati true
		String s4="Dva Studenta";
		
		System.out.println("s1=\""+s1+"\"");
		System.out.println("s4=\""+s4+"\"");
		if(s1==s4)
			System.out.println("== vratio true, ali s1 i s4 su u programu"+
					" eksp1icitno inicijalizovani istom String konstantom");
		else
			System.out.println("== vratio false.");
		
		System.out.println("*************************************************");
		System.out.println();

		//metod (compareTo() vrši leksikografsko poređenje dva Stringa
		//poredi se String za koji se poziva metod, sa Stringom koji
		//se prosleđuje kao argument
		//vraća cio broj 0 ako su jednaki
		//<0 ako je String za koji se poziva metod leksikografski MANJI od argumenta
		//>0 ako je String za koji se poziva metod leksikografski VEĆI od argumenta

		System.out.println("Leksikografsko poređenje dva stringa:");
		System.out.println(s1);
		System.out.println(s3);
		int rezLeksPor= s1.compareTo(s3);
		if(rezLeksPor==0)
			System.out.println("Leksikografski su jednaki");
		else if(rezLeksPor<0)
			System.out.println(s1 + " < "+s3);
		else
			System.out.println(s1 + " > "+s3);
		// mala slova imaju UNICODE vrijednosti veće od velikih slova

		System.out.println("*************************************************");
		System.out.println();

		// metod toUpperCase() vraća referencu na String dobijen
		// od polaznog Stringa konvertovanjem malih slova u velika
		// karakteri koji nisu mala slova ostaju nepromijenjeni		 

		String s5 = "programiranje 2 :";
		System.out.println("s5 = "+ s5);
		System.out.print("metod toUpperCase(): ");
		System.out.println(s5.toUpperCase());
		// i sam String s5 ostaje nepromijenjen,
		// vraća se referenca na novi String
		System.out.println("s5 = "+ s5);
	}
}
