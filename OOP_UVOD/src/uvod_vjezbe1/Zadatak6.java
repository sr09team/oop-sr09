package uvod_vjezbe1;

public class Zadatak6 {

	public static void main(String[] args) {

		System.out.println("Izdvajanje podstring-a");
		String s6="02.03.2011.";
		System.out.println(s6);
		// metod substring() može imati jedan argument
		// poziciju od koje pocinje izdvajanje
		// izdvaja se dio String—a od zadate pozicije do kraja
		System.out.println("Pocevši od 7. karaktera pa do kraja:");
		System.out.println(s6.substring(6));
		// postoji i verzija metoda koja prima 2 argumenta
		// prvi je takođe pozicija od koje pocinje izdvajanje
		// drugi je pozicija neposredno iza pozicije posljednjeg 
		// karaktera koji se izdvaja
		System.out.println("0d 7. do 10. karaktera:");
		System.out.println(s6.substring(6,10));
		
		System.out.println("*****************************************************");
		System.out.println();

		System.out.println("mama");
		System.out.println("zamjena karaktera 'm' karakterom 't' kroz citav String");
		System.out.println("mama".replace('m', 't'));

		System.out.println("*****************************************************");
		System.out.println();

		// uklanjanje eventualnih vodećih i krajnjih bjelina
		String s7=" string za trim-ovanje ";
		System.out.println("s7=\n\t*"+s7+"*");
		System.out.println("s7.trim()=\n\t*"+s7.trim()+"*");
		// rezultat je novi string dobijen odsjecanjem bjelina od s7, a
		// sam string s7 je nepromijenjen
		System.out.println("s7=\n\t*"+s7+"*");

		System.out.println("*****************************************************");
		System.out.println();

		String s8="0vo je string";
		String prefix="0vo ";
		String sufix="string";
		if(s8.startsWith(prefix))
			System.out.println("String \"" + s8 + "\" pocinje stringom \"" + prefix +"\"");
		else
			System.out.println("String \"" + 58 + "\" ne pocinje stringom \"" + prefix +"\"");
		if(s8.endsWith(sufix))
			System.out.println("String \"" + s8 + "\" se završava stringom \"" + sufix +"\"");
		else
			System.out.println("String \"" + s8 + "\" se ne završava stringom \"" + sufix +"\"");
		}
}

