/* modifikacija prethodnog primjera (ArSredina) sa unosom se staje kada se
unese max broj elemenata ili kada se unese 0 (u tom slucaju 0 se ne racuna
kao element niza) */

package uvod_vjezbe1;
import java.util.Scanner;

public class Zadatak4 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Unesite dim. niza");
		int n = sc.nextInt();

		int a[] = new int[n];

		System.out.println("Unesite elemente: ");
//		 U promjenljvoj ZBIR akumuliramo zbir unijetih elemenata
//		 lokalne promenljive moramo sami inicijalizovati
//		 njihova inicijalizacija ne vréi se automatski

		int zbir = 0;
		int i=0; 
//		 izvukli smo ovdje deklaraciju promjenjive i da bi se
//		 i poslije for-petlje, jer nam je potrebna za
//		 aritmetiéke sredine
		
		for (; i < n; i++) {
			a[i] = sc.nextInt();
			/******** NOVO *******/
			if(a[i]==0) break;
			/******** NOVO *******/

			zbir += a[i];
		}

		System.out.println("Zbir unjetih elemenata je " + zbir);
		double arSred = (double) zbir / n;
		System.out.println("Ar. sred. je " + arSred);
		sc.close();
	}
}
